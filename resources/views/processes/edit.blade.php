@extends('adminlte.default')

@section('title') Edit sub-process @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('processes.show',[(isset($process->pgroup) ? $process->pgroup->id : 0),$process])}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    {{Form::open(['url' => route('processes.update',[(isset($process->pgroup) ? $process->pgroup->id : 0),$process]), 'method' => 'post','class'=>'mt-3 mb-3','autocomplete'=>'off'])}}

    {{Form::label('name', 'Name')}}
    {{Form::text('name',$process->name,['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
    @foreach($errors->get('name') as $error)
        <div class="invalid-feedback">
            {{ $error }}
        </div>
    @endforeach

        {{Form::label('office', 'Office')}}
        {{Form::select('office',$offices,$process->office_id,['class'=>'form-control form-control-sm'. ($errors->has('office') ? ' is-invalid' : '')])}}
        @foreach($errors->get('office') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach

        {{Form::label('process', 'Process')}}
        {{Form::select('process',$process_groups,$process->process_group_id,['class'=>'form-control form-control-sm'. ($errors->has('process') ? ' is-invalid' : '')])}}
        @foreach($errors->get('process') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach

        {{Form::label('docfusion_process_id', 'DocFusion Process ID')}}
        {{Form::text('docfusion_process_id',$process->docfusion_process_id,['class'=>'form-control form-control-sm'. ($errors->has('docfusion_process_id') ? ' is-invalid' : ''),'placeholder'=>'DocFusion Process ID'])}}

        {{Form::label('docfusion_template_id', 'DocFusion Template ID')}}
        {{Form::text('docfusion_template_id',$process->docfusion_template_id,['class'=>'form-control form-control-sm'. ($errors->has('docfusion_template_id') ? ' is-invalid' : ''),'placeholder'=>'DocFusion Template ID'])}}

    <div class="row mt-3">
        <div class="col-lg-4 text-center">
            <label><i class="fa fa-circle" style="color: {{$process->getStageHex(0)}}"></i> Not started</label>
            <div class="form-control">
                <input name="not_started_colour" type="text" style="background-color:{{$process->getStageHex(0, false)}};" class="not_started_color form-control form-control-sm"  value="{{$process->getStageHex(1, false)}}" title="Stage colour: Not started"/>
            </div>
        </div>
        <div class="col-lg-4 text-center">
            <label><i class="fa fa-circle" style="color: {{$process->getStageHex(1)}}"></i> Started</label>
            <div class="form-control">
                <input name="started_colour" type="text"  style="background-color:{{$process->getStageHex(1, false)}};" class="started_colour form-control form-control-sm" value="{{$process->getStageHex(1, false)}}" title="Stage colour: Started"/>
            </div>
        </div>
        <div class="col-lg-4 text-center">
            <label><i class="fa fa-circle" style="color: {{$process->getStageHex(2)}}"></i> Completed</label>
            <div class="form-control">
                <input name="completed_colour" type="text" style="background-color:{{$process->getStageHex(2, false)}};" class="completed_colour form-control form-control-sm" value="{{$process->getStageHex(2,false)}}" title="Stage colour: Completed"/>
            </div>
        </div>
    </div>

    <div class="blackboard-fab mr-3 mb-3">
        <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-save"></i> Save</button>
    </div>


    {{-- todo notifications --}}

    {{Form::close()}}
    </div>
@endsection
@section('extra-js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.3.3/js/bootstrap-colorpicker.min.js"></script>
    <style>
        .colorpicker-2x .colorpicker-saturation {
            width: 200px;
            height: 200px;
        }

        .colorpicker-2x .colorpicker-hue,
        .colorpicker-2x .colorpicker-alpha {
            width: 30px;
            height: 200px;
        }

        .colorpicker-2x .colorpicker-color,
        .colorpicker-2x .colorpicker-color div {
            height: 30px;
        }
    </style>
    <script>
        $(function () {
            // Basic instantiation:
            $('.not_started_color').colorpicker({
                align: 'center',
                format: 'rgba',
                customClass: 'colorpicker-2x',
                sliders: {
                    saturation: {
                        maxLeft: 200,
                        maxTop: 200
                    },
                    hue: {
                        maxTop: 200
                    },
                    alpha: {
                        maxTop: 200
                    }
                }
            }).on('changeColor', function(event) {
                $('.not_started_color').css('background-color', event.color.toString());

            });

            $('.started_colour').colorpicker({
                align: 'center',
                format: 'rgba',
                customClass: 'colorpicker-2x',
                sliders: {
                    saturation: {
                        maxLeft: 200,
                        maxTop: 200
                    },
                    hue: {
                        maxTop: 200
                    },
                    alpha: {
                        maxTop: 200
                    }
                }
            }).on('changeColor', function(event) {
                $('.started_colour').css('background-color', event.color.toString());

            });

            $('.completed_colour').colorpicker({
                align: 'center',
                format: 'rgba',
                customClass: 'colorpicker-2x',
                sliders: {
                    saturation: {
                        maxLeft: 200,
                        maxTop: 200
                    },
                    hue: {
                        maxTop: 200
                    },
                    alpha: {
                        maxTop: 200
                    }
                }
            }).on('changeColor', function(event) {
                $('.completed_colour').css('background-color', event.color.toString());

            });
        });
    </script>
@endsection