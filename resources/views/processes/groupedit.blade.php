@extends('adminlte.default')

@section('title') Edit @foreach($process_groups as $process_group) {{$process_group->name}} @endforeach @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('processesgroup.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        @foreach($process_groups as $process_group)
        {{Form::open(['url' => route('processesgroup.update',$process_group->id), 'method' => 'post','class'=>'mt-3 mb-3','autocomplete' => 'off'])}}
        {{Form::label('name', 'Name')}}
        {{Form::text('name',$process_group->name,['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
        @foreach($errors->get('name') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach

        <div class="blackboard-fab mr-3 mb-3">
            <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-save"></i> Save</button>
        </div>

        {{Form::close()}}
        {{-- todo notifications --}}
    @endforeach
    </div>
@endsection
@section('extra-js')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
@endsection