@extends('adminlte.default')

@section('title') Create {{$type_name}} @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('processesgroup.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {{Form::open(['url' => route('processesgroup.store'), 'method' => 'post','class'=>'mt-3 mb-3','autocomplete' => 'off'])}}
        <input type="hidden" name="process_type_id" id="process_type_id" value="{{isset($process_type_id)?$process_type_id:1}}"/>
        {{Form::label('name', 'Name')}}
        {{Form::text('name',old('name'),['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
        @foreach($errors->get('name') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach

        <div class="blackboard-fab mr-3 mb-3">
            <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-save"></i> Save</button>
        </div>

        {{-- todo notifications --}}

        {{Form::close()}}
    </div>
@endsection
@section('extra-js')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
@endsection