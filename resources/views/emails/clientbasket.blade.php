<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
<strong>Dear client,</strong>

<p>Please find below the link to complete your application details. Please use the link provided below to access your profile. Enter your email address as username and the password below to access your profile. Please complete all required fields.</p>

<p>Password: <strong>{{$password}}</strong></p>

<p><a href="{{url('/client/'.$clientid.'/progress/'.$process_id.'/'.$step_id)}}">Click here</a></p>

<p><strong>Thank you in advance.</strong></p>

</body>

</html>