@extends('adminlte.default')

@section('title') {{$forms->name}} @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('forms.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-lg-9">
                <div class="card">
                    <div class="card-header">
                        Form Details
                    </div>
                    <div class="card-body">
                        <ul class="mr-0 pl-0">
                            <dt>
                                Sections
                            </dt>
                            <dd>
                                <div class="table-responsive ">
                                    <table class="table table-bordered table-sm table-hover">
                                        <thead class="btn-dark">
                                        <tr>
                                            <th>Name</th>
                                            <th>Fields</th>
                                            <th class="last">Move</th>
                                            <th class="last">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($forms->sections as $section)
                                            <tr>
                                                <td>{{$section->name}}</td>
                                                <td>{{$section->form_section_input()->count()}}</td>
                                                <td class="last">
                                                    <a href="#" title="Move activity up" onclick="document.querySelector('.moveform-up-{{$section->id}}').submit()"><i class="fa fa-arrow-up"></i></a>
                                                    |
                                                    <a href="#" title="Move activity down" onclick="document.querySelector('.moveform-down-{{$section->id}}').submit()"><i class="fa fa-arrow-down"></i></a>
                                                    {{Form::open(['url' => route('form_section.move',$section), 'method' => 'post','class'=>'moveform-up-'.$section->id])}}
                                                    {{Form::hidden('direction','up')}}
                                                    {{Form::close()}}
                                                    {{Form::open(['url' => route('form_section.move',$section), 'method' => 'post','class'=>'moveform-down-'.$section->id])}}
                                                    {{Form::hidden('direction','down')}}
                                                    {{Form::close()}}
                                                </td>
                                                <td class="last">
                                                    <a href="{{route('form_section.edit',$section)}}" class="btn btn-success btn-sm">Edit</a>
                                                    <a href="#" onclick="document.querySelector('.deleteform-{{$section->id}}').submit()" class="btn btn-danger btn-sm">Delete</a>
                                                    {{Form::open(['url' => route('form_section.destroy',$section), 'method' => 'delete','class'=>'deleteform-'.$section->id])}}
                                                    {{Form::close()}}
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="100%" class="text-center">
                                                    <small class="text-muted">No Sections created yet.</small>
                                                </td>
                                            </tr>
                                        @endforelse
                                        <tr>
                                            <td class="text-center" colspan="100%">
                                                <a href="{{route('form_section.create',$forms)}}" class="btn btn-sm btn-success">
                                                    Add
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </dd>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="col-lg-3">
                <div class="card">
                    <div class="card-header">
                        Actions
                    </div>
                    <div class="card-body">
                        {{--<button type="button" id="view_variables" class="btn btn-block btn-sm btn-outline-dark"><i class="far fa-eye"></i> View Variables</button>
                        <br />--}}
                        <br />
                        @if(auth()->user()->can('admin'))
                            <a href="{{route('forms.edit',$forms)}}" class="btn btn-sm btn-block btn-outline-success"><i class="fa fa-pencil-alt"></i> Edit</a>

                            <br>

                            {{Form::open(['url' => route('forms.destroy',['form' => $forms,'formid' => $forms]), 'method' => 'delete'])}}
                            <button type="submit" class="btn btn-block btn-sm btn-outline-danger"><i class="fa fa-trash"></i> Delete</button>
                            {{Form::close()}}
                        @else
                            <button type="button" class="btn btn-block btn-sm btn-outline-primary disabled" disabled title="You do not have permission to do that"><i class="fa fa-pencil"></i> Edit</button>

                            <br>

                            <button type="button" class="btn btn-block btn-sm btn-outline-danger disabled" disabled title="You do not have permission to do that"><i class="fa fa-trash"></i> Delete</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalViewVariables" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="row">
                        <div class="alert alert-primary mt-3">
                            <div class="form-group form-inline">
                                {{Form::label('section_id', 'Section Filter:')}}&nbsp;&nbsp;
                                {{Form::select('section_id',$form_sections,null,['id'=>'section_id', 'class'=>'form-control form-control-sm col-sm-4 form-inline'])}}
                            </div>
                            <table class="table table-responsive">
                                <thead>
                                <tr>
                                    <th>Form Section</th>
                                    <th>Form Field</th>
                                    <th>Variable</th>
                                </tr>
                                </thead>
                                <tbody id="section_input" style="height: 300px;overflow-y: auto;display:block;overflow-x: hidden;">
                                <tr>
                                    @foreach($formfields as $sections)
                                        @foreach($sections["form_section_input"] as $section)
                                            @php
                                                $var = strtolower(str_replace(' ','_',$sections->name)).'.'.$section["order"];
                                            @endphp
                                            <td width="33.33%">{{$sections->name}}</td><td width="33.33%"><i>{{$section["name"]}}</i></td><td width="33.33%">$&#123;{{$var}}&#125;</td>
                                            @if(($loop->iteration)%1 == 0 && $loop->iteration != 0)
                                </tr><tr>
                                @endif
                                @endforeach
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row text-center col-sm-12">
                            <button class="btn btn-sm btn-default" id="viewvariables_cancel" style="margin: 0px auto;">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-css')
    <style>
        #modalViewVariables table {
            flex-flow: column;
            height: 100%;
            width: 100%;
        }
        #modalViewVariables table thead {
            /* head takes the height it requires,
            and it's not scaled when table is resized */
            flex: 0 0 auto;
            width: calc(100% - 0.9em);
        }
        #modalViewVariables table tbody {
            /* body takes all the remaining available space */
            flex: 1 1 auto;
            display: block;
            overflow-y: scroll;
        }
        #modalViewVariables table tbody tr {
            width: 100%;
        }
        #modalViewVariables table thead, #modalViewVariables table tbody tr {
            display: table;
            table-layout: fixed;
        }
        .modal-dialog {
            width: 800px;
            max-width: 800px;
            margin: 30px auto;
        }
    </style>
@endsection
@section('extra-js')
    <script>
        $(function () {
            $("#view_variables").on("click",function(){
                $("#modalViewVariables").modal('show');
            })

            $("#viewvariables_cancel").on("click",function () {
                $("#modalViewVariables").modal('hide');
            })

            $("#section_id").on('change',function (){
                let form_id = $("#form_id").val();
                let section_id = $("#section_id").val();

                $.ajax({
                    url: '/forms_section/' + form_id + '/get_inputs/' + section_id,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        let rows = '<tr>';
                        let count = 0;
                        $.each(data, function (key, value) {
                            count++;
                            rows = rows + '<td width="33.33%">' + value.form_section + '</td>';
                            rows = rows + '<td width="33.33%">' + value.form_input + '</td>';
                            rows = rows + '<td width="33.33%">$&#123;' + value.form_variable + '&#125;</td>';
                            if (count === 1) {
                                rows = rows + '</tr><tr>';
                                count = 0;
                            }
                        });

                        $("#modalViewVariables").find("#section_input").html(rows);
                    }
                });
            })
        })
    </script>
@endsection