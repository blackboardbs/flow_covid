@extends('adminlte.default')

@section('title') Edit Form Section @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        {{--<a href="{{route('processes.show',$step->process)}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>--}}
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="card mt-3">
            <div class="card-body">
                <dt>
                    Form
                </dt>
                <dd>
                    {{$form->form->name}}
                </dd>

            </div>
        </div>

        {{Form::open(['url' => route('form_section.update',$form), 'method' => 'put','class'=>'mt-3 mb-3'])}}

        {{Form::label('name', 'Name')}}
        {{Form::text('name',$form->name,['class'=>'form-control form-control-sm '. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
        @foreach($errors->get('name') as $error)
            <div class="invalid-feedback">
                {{ $error }}
            </div>
        @endforeach

        <hr>

        {{Form::label('activities', 'Activities')}}
        <blackboard-forms-editor
                :black-inputs="{{$inputs}}"

        ></blackboard-forms-editor>

        <div class="blackboard-fab mr-3 mb-3">
            <button type="submit" class="btn btn-primary btn-lg submitbtn"><i class="fa fa-save"></i> Save</button>
        </div>

        {{Form::close()}}
    </div>
@endsection