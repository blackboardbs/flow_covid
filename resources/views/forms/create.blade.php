@extends('adminlte.default')

@section('title') Create Form @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('forms.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        {{Form::open(['url' => route('forms.store'), 'method' => 'post','class'=>'mt-3 mb-3','files'=>true,'autocomplete' => 'off'])}}
        <div class="form-group">
            {{Form::label('name', 'Name')}}
            {{Form::text('name',old('name'),['class'=>'form-control form-control-sm'. ($errors->has('name') ? ' is-invalid' : ''),'placeholder'=>'Name'])}}
            @foreach($errors->get('name') as $error)
                <div class="invalid-feedback">
                    {{ $error }}
                </div>
            @endforeach
        </div>

        {{--{{Form::label('fields', 'Fields')}}
        <blackboard-forms-editor></blackboard-forms-editor>--}}

        <div class="blackboard-fab mr-3 mb-3">
            <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-save"></i> Save</button>
        </div>

        {{-- todo notifications --}}

        {{Form::close()}}
    </div>
@endsection
