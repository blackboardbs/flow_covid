@extends('adminlte.default')

@section('title') Recents @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
    <div class="row mt-3">
        <div class="col-lg-12">
            <div class="table-responsive ">
                <table class="table table-sm table-bordered blackboard-recents">
                    <thead>
                    <tr>
                        <th>
                            Recent Client Visits
                            <a href="{{route('clients.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-eye"></i> View all</a>
                        </th>
                    </tr>
                    </thead>
                    @forelse($clients as $client)
                        <tr>
                            <td>
                                <a href="{{route('clients.show',[$client->client_id,$client->process_id,$client->step_id])}}">{{$client->client->first_name}} {{$client->client->last_name}}</a>
                                <span class="float-right text-muted"><small><i class="fa fa-clock-o"></i> {{$client->updated_at}}</small></span>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>No recent visits</td>
                        </tr>
                    @endforelse
                </table>
            </div>
        </div>
    </div>
    </div>
@endsection
