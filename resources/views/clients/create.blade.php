@extends('adminlte.default')

@section('title')
    Capture Visit
@endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @auth
        <a href="{{route('clients.index')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
        @endauth
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        @if(isset($forms) && count($forms) > 0)
        <nav class="tabbable">
            <div class="nav nav-pills">
                <a class="nav-link active show" id="default-tab" data-toggle="tab" href="#default" role="tab" aria-controls="default" aria-selected="false">Default</a>
                @foreach($forms as $key =>$value)
                    @foreach($value as $section =>$v1)
                        <a class="nav-link" id="{{strtolower(str_replace(' ','_',$section))}}-tab" data-toggle="tab" href="#{{strtolower(str_replace(' ','_',$section))}}" role="tab" aria-controls="{{strtolower(str_replace(' ','_',$section))}}" aria-selected="true">{{$section}}</a>
                    @endforeach
                @endforeach
            </div>
        </nav>
        @endif
        {{Form::open(['url' => route('clients.store'), 'method' => 'post','autocomplete'=>'off'])}}
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active p-3" id="default" role="tabpanel" aria-labelledby="default-tab">
                <input type="hidden" name="office_id" value="{{(isset($office_id) ? $office_id : auth()->user()->office()->id)}}" />
                <div class="row mt-3">
                    <div class="col-lg-12">
                        <div class="form-group">
                            {{Form::label('first_name', 'Name')}}
                            {{Form::text('first_name',old('first_name'),['class'=>'form-control form-control-sm'. ($errors->has('first_name') ? ' is-invalid' : ''),'placeholder'=>'First Name'])}}
                            @foreach($errors->get('first_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group">
                            {{Form::label('email', 'Email')}}
                            {{Form::email('email',old('email'),['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                            @foreach($errors->get('email') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group">
                            {{Form::label('contact', 'Cellphone Number')}}
                            {{Form::text('contact',old('contact'),['class'=>'form-control form-control-sm'. ($errors->has('contact') ? ' is-invalid' : ''),'placeholder'=>'Contact Number'])}}
                            @foreach($errors->get('contact') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group">
                            {{Form::label('accompanied', 'Are you accompanied by children?')}}
                            <div class="form-group">
                                <label class="radio-inline"><input type="radio" name="accompanied" value="1"><span class="ml-2">Yes</span></label>
                                <label class="radio-inline ml-3"><input type="radio" name="accompanied" value="0"><span class="ml-2">No</span></label>
                                @foreach($errors->get('accompanied') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group accompanied_before" style="display: none;">
                            {{Form::label('accompanied_before', 'Have you been accompanied by any of these children before?')}}
                            <div class="form-group">
                                <label class="radio-inline"><input type="radio" name="accompanied_before" value="1"><span class="ml-2">Yes</span></label>
                                <label class="radio-inline ml-3"><input type="radio" name="accompanied_before" value="0"><span class="ml-2">No</span></label>
                                @foreach($errors->get('accompanied_before') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group nr_accompanied" style="display: none;">
                            {{Form::label('nr_accompanied', 'Number of accompanying children')}}
                            <select name="nr_accompanied" class="chosen-select form-control form-control-sm">
                                <option value="0">Please Select</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                            @foreach($errors->get('nr_accompanied') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </div>

                    </div>
                </div>

            </div>

        </div>
            <div class="blackboard-fab mb-3">
                <button type="submit" class="btn btn-info btn-lg process-submit"><i class="fa fa-arrow-right"></i><span class="save_span">Next</span></button>
            </div>
        {{Form::close()}}
    </div>

@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        a:focus{
            outline:none !important;
            border:0px !important;
        }

        .activity a{
            color: rgba(0,0,0,0.5) !important;
        }

        .activity a.dropdown-item {
            color:#212529 !important;
        }

        .btn-comment{
            padding: .25rem .25rem;
            font-size: .575rem;
            line-height: 1;
            border-radius: .2rem;
        }

        .modal-dialog {
            max-width: 700px;
            margin: 1.75rem auto;
        }

        .chosen-container, .chosen-container-multi{
            width:100% !important;
        }

        .chosen-container, .chosen-container-multi{
            line-height: 30px;
        }

        .modal-open .modal{
            padding-right: 0px !important;
        }

        .progress { position:relative; width:100%; border: 1px solid #7F98B2; padding: 1px; border-radius: 3px; display:none; }
        .bar { background-color: #B4F5B4; width:0%; height:25px; border-radius: 3px; }
        .percent { position:absolute; display:inline-block; top:3px; left:48%; color: #7F98B2;}
    </style>
@endsection
@section('extra-js')
    <script>

        $(document).ready(function(){
            $('input[type=radio][name=accompanied]').on('click',function() {
                if (this.value === '1') {
                    $('.accompanied_before').show();
                    $('.nr_accompanied').show();
                }
                else if (this.value === '0') {
                    $('.accompanied_before').hide();
                    $('.nr_accompanied').hide();
                }
            });
        })

    </script>
@endsection