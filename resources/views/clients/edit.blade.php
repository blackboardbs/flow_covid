@extends('adminlte.default')

@section('title') Confirm Details @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        @auth
        <a href="{{route('clients.show',[$client,$process_id,$step['id']])}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-caret-left"></i> Back</a>
        @endauth
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        @if(count($forms) > 0)
        <nav class="tabbable">
            <div class="nav nav-pills">
                <a class="nav-link active show" id="default-tab" data-toggle="tab" href="#default" role="tab" aria-controls="default" aria-selected="false">Default</a>
                @foreach($forms as $key =>$value)
                    @foreach($value as $section =>$v1)
                        <a class="nav-link" id="{{strtolower(str_replace(' ','_',$section))}}-tab" data-toggle="tab" href="#{{strtolower(str_replace(' ','_',$section))}}" role="tab" aria-controls="{{strtolower(str_replace(' ','_',$section))}}" aria-selected="true">{{$section}}</a>
                    @endforeach
                @endforeach
            </div>
        </nav>
        @endif
        {{Form::open(['url' => route('clients.update', $client), 'method' => 'put','autocomplete'=>'off','id'=>'editDetail'])}}
        <input type="hidden" id="form_id" value="2"/>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active p-3" id="default" role="tabpanel" aria-labelledby="default-tab">
                <input type="hidden" name="office_id" value="{{(isset($office_id) ? $office_id : (auth()->check() ? auth()->user()->office()->id : ''))}}" />
    <div class="form-group">
        {{Form::label('first_name', 'Name')}}
        {{Form::text('first_name',$client->first_name,['class'=>'form-control form-control-sm'. ($errors->has('first_name') ? ' is-invalid' : ''),'placeholder'=>'First Name'])}}
        @foreach($errors->get('first_name') as $error)
            <div class="invalid-feedback">
                {{$error}}
            </div>
        @endforeach
    </div>

        <div class="form-group">
            {{Form::label('email', 'Email')}}
            {{Form::email('email',$client->email,['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
            @foreach($errors->get('email') as $error)
                <div class="invalid-feedback">
                    {{$error}}
                </div>
            @endforeach
        </div>

        <div class="form-group">
            {{Form::label('contact', 'Cellphone Number')}}
            {{Form::text('contact',$client->contact,['class'=>'form-control form-control-sm'. ($errors->has('contact') ? ' is-invalid' : ''),'placeholder'=>'Contact Number'])}}
            @foreach($errors->get('contact') as $error)
                <div class="invalid-feedback">
                    {{$error}}
                </div>
            @endforeach
        </div>

                <div class="form-group">
                    {{Form::label('accompanied', 'Are you accompanied by children?')}}
                    <div class="form-group">
                        <label class="radio-inline"><input type="radio" name="accompanied" value="1"><span class="ml-2">Yes</span></label>
                        <label class="radio-inline ml-3"><input type="radio" name="accompanied" value="0"><span class="ml-2">No</span></label>
                        @foreach($errors->get('accompanied') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="form-group accompanied_before" style="display: none;">
                    {{Form::label('accompanied_before', 'Have you been accompanied by any of these children before?')}}
                    <div class="form-group">
                        <label class="radio-inline"><input type="radio" name="accompanied_before" value="1"><span class="ml-2">Yes</span></label>
                        <label class="radio-inline ml-3"><input type="radio" name="accompanied_before" value="0"><span class="ml-2">No</span></label>
                        @foreach($errors->get('accompanied_before') as $error)
                            <div class="invalid-feedback">
                                {{$error}}
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="form-group nr_accompanied" style="display: none;">
                    {{Form::label('nr_accompanied', 'Number of accompanying children')}}
                    <input type="hidden" name="previous_guests" id="previous_guests" />
                    <select name="nr_accompanied" class="chosen-select form-control form-control-sm" id="nr_accompanied">
                        <option value="0">Please Select</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                    </select>
                    @foreach($errors->get('nr_accompanied') as $error)
                        <div class="invalid-feedback">
                            {{$error}}
                        </div>
                    @endforeach
                </div>
    </div>


            </div>
            <div class="blackboard-fab mb-3">
                <button type="submit" class="btn btn-info btn-lg process-submit"><i class="fa fa-arrow-right"></i><span class="save_span">Next</span></button>
            </div>
            {{Form::close()}}
    </div>
    <div class="modal fade" id="modalProcess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="row">
                        <div class="md-form col-sm-12 mb-3 text-left message">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalGuests" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="row">
                        <input type="hidden" id="numguests">
                        Please select which of the below children you are accompanied by:
                        <div class="md-form col-sm-12 mb-3 text-left guests">

                        </div>
                    </div>
                    <div class="row">
                        <button type="button" class="btn btn-default btn-sm pull-left" id="guestsdone">Done</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <style>
        a:focus{
            outline:none !important;
            border:0px !important;
        }

        .activity a{
            color: rgba(0,0,0,0.5) !important;
        }

        .activity a.dropdown-item {
            color:#212529 !important;
        }

        .btn-comment{
            padding: .25rem .25rem;
            font-size: .575rem;
            line-height: 1;
            border-radius: .2rem;
        }

        .modal-dialog {
            max-width: 700px;
            margin: 1.75rem auto;
        }

        .chosen-container, .chosen-container-multi{
            width:100% !important;
        }

        .chosen-container, .chosen-container-multi{
            line-height: 30px;
        }

        .modal-open .modal{
            padding-right: 0px !important;
        }

        .progress { position:relative; width:100%; border: 1px solid #7F98B2; padding: 1px; border-radius: 3px; display:none; }
        .bar { background-color: #B4F5B4; width:0%; height:25px; border-radius: 3px; }
        .percent { position:absolute; display:inline-block; top:3px; left:48%; color: #7F98B2;}
    </style>
@endsection
@section('extra-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script>
        $(function () {
            $(document).on('click','.guestid',function () {
                if($('#modalGuests').find('.guestid:checked').length >= $("#modalGuests").find('#numguests').val()){
                    $('#modalGuests').find('.guestid').attr("disabled", true)
                }
            })

            $('#guestsdone').on('click',function () {
                var guestIDs = $('#modalGuests').find(".guests input:checkbox:checked").map(function(){
                    return $(this).val();
                }).get();

                $('#previous_guests').val(guestIDs.toString());
                $("#modalGuests").modal('hide');
                $('#editDetail')[0].submit(function(){
                    alert("Submitted");
                });
            })
            $("form").submit(function(e){
                e.preventDefault(e);
                if($('input[type=radio][name=accompanied_before]:checked').val() === '1'){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        dataType: 'json',
                        url: '/getguests/'+{{$client->id}},
                        type:'GET'
                    }).done(function(data) {
                        //alert(Object.keys(data).length);
                        if(Object.keys(data).length > 0) {
                            $("#modalGuests").modal('show');
                            $("#modalGuests").find('#numguests').val();
                            $("#modalGuests").find('.guests').empty();
                            let row = '';
                            $.each(data, function (key, value) {
                                row = row + '<input type="checkbox" class="guestid" value="' + key + '"><span style="padding-left:10px;">' + value + '</span><br />';
                            });
                            $("#modalGuests").find('#numguests').val($(document).find('#nr_accompanied').val());
                            $("#modalGuests").find('.guests').html(row);
                        } else {
                            $('#editDetail')[0].submit(function(){
                                alert("Submitted");
                            });
                        }

                    });
                } else {
                    $('#editDetail')[0].submit(function(){
                        alert("Submitted");
                    });
                }
            });

            $('input[type=radio][name=accompanied]').on('click',function() {
                if (this.value === '1') {
                    $('.accompanied_before').show();
                    $('.nr_accompanied').show();
                }
                else if (this.value === '0') {
                    $('.accompanied_before').hide();
                    $('.nr_accompanied').hide();
                }
            });
        })
        $(function(){

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                let total = $('.active .select-this').length;
                let total_selected = $('.active .select-this:checked').length;

                if(total === total_selected){
                    $(".select-all").prop('checked',true);
                } else {
                    $(".select-all").prop('checked',false);
                }
            });

            $(".select-all").on('change',function(){  //"select all" change
                var status = this.checked; // "select all" checked status
                var cnt = 1;

                $('.active .select-this').each(function(){ //iterate all listed checkbox items
                    this.checked = status; //change ".checkbox" checked status
                    var status_id = $(".active .select-all").prop('checked') == true ? 1 : 0;;
                    var input = $(this).val();
                    var form_id = $('#form_id').val();
                    var client_id = $(this).data('client');
                    var section = $('.active .section ').val();


                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: '/forms/include-in-basket',
                        data: {'status': status_id, 'input_id':input, 'client_id': client_id,'form_id':form_id,'section':section,'all':'1'},
                        success: function(data) {
                            if (status_id) {
                                cnt = cnt + 1;
                                if (cnt === $(".select-this:checked").length) {
                                    toastr.success(data.success);
                                }
                            }

                            if (!status_id && cnt === 1) {
                                cnt = cnt + 1;
                                toastr.warning(data.success);
                            }
                            toastr.options.timeOut = 500;

                        }
                    });
                });
            });

            $('.select-this').on('change',function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                let total = $('.active .select-this').length;
                let total_selected = $('.active .select-this:checked').length;

                if(total === total_selected){
                    $(".select-all").prop('checked',true);
                } else {
                    $(".select-all").prop('checked',false);
                }

                var status = $(this).prop('checked') == true ? 1 : 0;
                var input = $(this).val();
                var form_id = $('#form_id').val();
                var client_id = $(this).data('client');

                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: '/forms/include-in-basket',
                    data: {'status': status, 'input_id':input, 'client_id': client_id,'form_id':form_id},
                    success: function(data){
                        if(status){
                            toastr.success(data.success);
                        }else{
                            toastr.warning(data.success);
                        }
                        toastr.options.timeOut = 500;
                    }
                });
            })

            $('#process').on('change',function(){
                $.ajax({
                    dataType: 'json',
                    url: '/processes/step_count/'+$('#process').val(),
                    type:'GET'
                }).done(function(data) {
                    if(data === 0){
                        let process = $('#process').val();
                        $('#modalProcess').modal('show');
                        $('#modalProcess').find('.message').html('The process you selected currently has no steps.<br /><br />' +
                            'Click <a href="/processes/' + process + '/show">here</a> to add steps to this process now.');
                        $('body').find('.update-client').prop('disabled',true);
                    } else {
                        $('body').find('.update-client').prop('disabled',false);
                    }
                });
            })


        })
    </script>
@endsection