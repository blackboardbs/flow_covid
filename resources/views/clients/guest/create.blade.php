@extends('adminlte.default')

@section('title')
    Capture Visit
@endsection

@section('header')
    <div class="container-fluid container-title">
        @if($is_valid == 1)
            <h3>@yield('title')</h3>
        @endif
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        @if($is_valid == 1)
        @if(count($forms) > 0)
            <nav class="tabbable">
                <div class="nav nav-pills">
                    <a class="nav-link active show" id="default-tab" data-toggle="tab" href="#default" role="tab" aria-controls="default" aria-selected="false">Default</a>
                    @foreach($forms as $key =>$value)
                        @foreach($value as $section =>$v1)
                            <a class="nav-link" id="{{strtolower(str_replace(' ','_',$section))}}-tab" data-toggle="tab" href="#{{strtolower(str_replace(' ','_',$section))}}" role="tab" aria-controls="{{strtolower(str_replace(' ','_',$section))}}" aria-selected="true">{{$section}}</a>
                        @endforeach
                    @endforeach
                </div>
            </nav>
        @endif
        {{Form::open(['url' => route('clients.store'), 'method' => 'post','autocomplete'=>'off'])}}
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active p-3" id="default" role="tabpanel" aria-labelledby="default-tab">

                <div class="row mt-3">
                    <div class="col-lg-12">
                        <input type="hidden" name="office_id" value="{{isset($_GET['office'])?$_GET['office']:''}}" />
                        <div class="form-group">
                            {{Form::label('first_name', 'First Names')}}
                            {{Form::text('first_name',old('first_name'),['class'=>'form-control form-control-sm'. ($errors->has('first_name') ? ' is-invalid' : ''),'placeholder'=>'First Name'])}}
                            @foreach($errors->get('first_name') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group">
                            {{Form::label('email', 'Email')}}
                            {{Form::email('email',(old('email') ? old('email') : (Session::has('emaill') ? Session::get('emaill') : '')),['class'=>'form-control form-control-sm'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email'])}}
                            @foreach($errors->get('email') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group">
                            {{Form::label('contact', 'Cellphone Number')}}
                            {{Form::text('contact',old('contact'),['class'=>'form-control form-control-sm'. ($errors->has('contact') ? ' is-invalid' : ''),'placeholder'=>'Contact Number'])}}
                            @foreach($errors->get('contact') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group">
                            {{Form::label('accompanied', 'Are you accompanied by children?')}}
                            <div class="form-group">
                                <label class="radio-inline"><input type="radio" name="accompanied" value="1"><span class="ml-2">Yes</span></label>
                                <label class="radio-inline ml-3"><input type="radio" name="accompanied" value="0"><span class="ml-2">No</span></label>
                                @foreach($errors->get('accompanied') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group accompanied_before" style="display: none;">
                            {{Form::label('accompanied_before', 'Have you been accompanied by any of these children before?')}}
                            <div class="form-group">
                                <label class="radio-inline"><input type="radio" name="accompanied_before" value="1"><span class="ml-2">Yes</span></label>
                                <label class="radio-inline ml-3"><input type="radio" name="accompanied_before" value="0"><span class="ml-2">No</span></label>
                                @foreach($errors->get('accompanied_before') as $error)
                                    <div class="invalid-feedback">
                                        {{$error}}
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group nr_accompanied" style="display: none;">
                            {{Form::label('nr_accompanied', 'Number of accompanying children')}}
                            <select name="nr_accompanied" class="chosen-select form-control form-control-sm">
                                <option value="0">Please Select</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                            @foreach($errors->get('nr_accompanied') as $error)
                                <div class="invalid-feedback">
                                    {{$error}}
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
            @if(isset($forms) && count($forms) > 0)
                @foreach($forms as $key =>$value)
                    @foreach($value as $section =>$v1)
                        <div class="tab-pane fade p-3" id="{{strtolower(str_replace(' ','_',$section))}}" role="tabpanel" aria-labelledby="{{strtolower(str_replace(' ','_',$section))}}-tab" style="padding-bottom: 70px !important;">
                            @foreach($v1 as $k1 =>$inputs)

                                @foreach($inputs["inputs"] as $input)
                                    @if($input['type'] == 'dropdown')
                                        @php

                                            $arr = (array)$input['dropdown_items'];
                                            $arr2 = (array)$input['dropdown_values'];

                                        @endphp
                                        <input type="hidden" id="old_{{$input['id']}}" name="old_{{$input['id']}}" value="{{(!empty($arr2) ? implode(',',$arr2) : old($input['id']))}}">
                                    @else
                                        <input type="hidden" id="old_{{$input['id']}}" name="old_{{$input['id']}}" value="{{old($input['id'])}}">
                                    @endif
                                    @if($input['type']=='heading')
                                        <h4 style="width:{{$input['level']}}%;margin-left: calc(100% - {{$input['level']}}%);background-color:{{$input['color'] != 'hsl(0,0%,0%)' ? $input['color'] : ''}};padding:5px;">{{$input['name']}}</h4>
                                    @elseif($input['type']=='subheading')
                                        <h5 style="width:{{$input['level']}}%;margin-left: calc(100% - {{$input['level']}}%);background-color:{{$input['color'] != 'hsl(0,0%,0%)' ? $input['color'] : ''}};padding:5px;">{{$input['name']}}</h5>
                                    @else
                                        <div class="list-group-item" style="display:inline-block;width:{{$input['level']}}%;margin-left: calc(100% - {{$input['level']}}%);background-color:{{$input['color'] != 'hsl(0,0%,0%)' && $input['color'] != null ? $input['color'] : '#f5f5f5'}};border:1px solid rgba(0,0,0,.125);">
                                            <div style="display:inline-block;width:20px;vertical-align:top;"><i class="fa fa-circle" style="color: {{isset($input['value']) && $input['value'] != null ? 'rgba(50, 193, 75, 0.7)' : 'rgba(242, 99, 91, 0.7)'}}"></i> </div>
                                            <div style="display: inline-block;width: calc(100% - 25px)">
                                    <span style="width:88%;float: left;display:block;">
                                    {{$input["name"]}}
                                        <small class="text-muted"> [{{$input['type_display']}}] @if($input['kpi']==1) <span class="fa fa-asterisk" title="Activity is required for step completion" style="color:#FF0000"></span> @endif</small>
                                    </span>

                                                <div style="float: right;margin-right:5px; display: inline-block;margin-top: -3px;padding-bottom: 3px;text-align: right;" class="form-inline clientbasket">
                                                    <input type="checkbox" class="form-check-input" name="add_to_basket[]" id="{{$input['id']}}" value="{{$input['id']}}">
                                                    <label  for="{{$input['id']}}" class="form-check-label" style="font-weight:normal !important;"> </label>
                                                </div>
                                                @if($input['type']=='text')
                                                    {{Form::text($input['id'],old($input['id']),['class'=>'form-control form-control-sm','placeholder'=>'Insert text...','spellcheck'=>'true'])}}
                                                @endif

                                                @if($input['type']=='percentage')
                                                    <input type="number" min="0" step="1" max="100" name="{{$input['id']}}" value="{{(isset($input['value']) ? $input['value'] : old($input['id']))}}" class="form-control form-control-sm" spellcheck="true" />
                                                @endif

                                                @if($input['type']=='integer')
                                                    <input type="number" min="0" step="1" name="{{$input['id']}}" value="{{(isset($input['value']) ? $input['value'] : old($input['id']))}}" class="form-control form-control-sm" spellcheck="true" />
                                                @endif

                                                @if($input['type']=='amount')
                                                    <input type="number" min="0" step="1" name="{{$input['id']}}" value="{{(isset($input['value']) ? $input['value'] : old($input['id']))}}" class="form-control form-control-sm" spellcheck="true" />
                                                @endif

                                                @if($input['type']=='date')
                                                    <input name="{{$input['id']}}" type="date" min="1900-01-01" max="2030-12-30" value="{{old($input['id'])}}" class="form-control form-control-sm" placeholder="Insert date..." />
                                                @endif

                                                @if($input['type']=='textarea')
                                                    <textarea spellcheck="true" rows="5" name="{{$input['id']}}" class="form-control form-control-sm text-area"></textarea>
                                                @endif

                                                @if($input['type']=='boolean')
                                                    {{Form::select($input['id'],[1=>'Yes',0=>'No'],old($input['id']),['class'=>'form-control form-control-sm','placeholder'=>'Please select...'])}}
                                                @endif
                                                @if($input['type']=='dropdown')

                                                    <select multiple="multiple" id="{{$input['id']}}" name="{{$input["id"]}}[]" class="form-control form-control-sm chosen-select">
                                                        @php
                                                            foreach((array) $arr as $key => $value){
                                                                echo '<option value="'.$key.'" '.(in_array($key,$arr2) ? 'selected' : '').'>'.$value.'</option>';
                                                            }
                                                        @endphp
                                                    </select>
                                                    <div>
                                                        <small class="form-text text-muted">
                                                            Search and select multiple entries
                                                        </small>
                                                    </div>

                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @endforeach
                        </div>
                    @endforeach
                @endforeach
            @endif
        </div>
            <div class="blackboard-fab mb-3">
                <button type="submit" class="btn btn-info btn-lg process-submit"><i class="fa fa-arrow-right"></i><span class="save_span">Next</span></button>
            </div>
        {{Form::close()}}
        @else
            <div class="row m-0 pt-3 pb-5 border-0 border-top-0 activity-container">
            {{Form::open(['url' => route('clients.create'), 'method' => 'post','files'=>true,'style'=>'margin:0 auto;width:350px;'])}}
                <input type="hidden" name="office_id" value="{{isset($_GET['office'])?$_GET['office']:''}}" />
            <div class="form-group">
                <div class="alert alert-info">
                    Please enter your email below to continue.
                </div>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="fa fa-user"></i>
                        </div>
                    </div>
                    {{Form::email('email',($email != '' ? $email : old('email')),['class'=>'form-control'. ($errors->has('email') ? ' is-invalid' : ''),'placeholder'=>'Email','required','autofocus'])}}
                </div>
            </div>

            {{--<div class="form-group">
                <div class="input-group">
                    {!! app('captcha')->render() !!}
                </div>
            </div>--}}

            <div class="form-group">
                <button type="submit" class="btn btn-info" style="width: 100%; float:left;">
                    Continue
                </button>
                {{--<a href="javascript:void(0)" onclick="scanQR()" class="btn btn-secondary" style="width: 49%; float:right;">
                    Scan QR Code
                </a>--}}

            </div>
            {{Form::close()}}

            </div>
            {{--<div id="scan-qr-div" style="display: none">
                <blackboard-qrcode></blackboard-qrcode>
            </div>--}}
        @endif
    </div>
    <div class="modal fade" id="modalProcess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="row">
                        <div class="md-form col-sm-12 mb-3 text-left message">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>
        a:focus{
            outline:none !important;
            border:0px !important;
        }

        .activity a{
            color: rgba(0,0,0,0.5) !important;
        }

        .activity a.dropdown-item {
            color:#212529 !important;
        }

        .btn-comment{
            padding: .25rem .25rem;
            font-size: .575rem;
            line-height: 1;
            border-radius: .2rem;
        }

        .modal-dialog {
            max-width: 700px;
            margin: 1.75rem auto;
            min-width: 500px;
        }

        .chosen-container, .chosen-container-multi{
            width:100% !important;
        }

        .chosen-container, .chosen-container-multi{
            line-height: 30px;
        }

        .modal-open .modal{
            padding-right: 0px !important;
        }

        .progress { position:relative; width:100%; border: 1px solid #7F98B2; padding: 1px; border-radius: 3px; display:none; }
        .bar { background-color: #B4F5B4; width:0%; height:25px; border-radius: 3px; }
        .percent { position:absolute; display:inline-block; top:3px; left:48%; color: #7F98B2;}

        .decoded-content {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            max-width: 100%;

            color: white;
            font-weight: bold;
            padding: 10px;
            background-color: rgba(0,0,0,.5);
        }

        .error {
            color: red;
            font-weight: bold;
        }
    </style>
@endsection
@section('extra-js')
    <script>

        $(document).ready(function(){
            $('input[type=radio][name=accompanied]').on('click',function() {
                if (this.value === '1') {
                    $('.accompanied_before').show();
                    $('.nr_accompanied').show();
                }
                else if (this.value === '0') {
                    $('.accompanied_before').hide();
                    $('.nr_accompanied').hide();
                }
            });
        })

    </script>
@endsection