@extends('clients.show')

@section('tab-content')
    <div class="col-lg-12">

        <a href="{{route('documents.create',['client'=>$client->id,'process_id'=>$process_id,'step_id'=>$step["id"]])}}" class="btn btn-outline-primary btn-sm"><i class="fa fa-plus"></i> Document</a>

        <div class="table-responsive mt-3">
            <table class="table table-bordered table-sm table-hover">
                <thead class="btn-dark">
                <tr>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Size</th>
                    <th>Uploader</th>
                    <th>Added</th>
                    <th class="last">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($client->documents as $document)
                    <tr>
                        <td><a href="{{route('document',['q'=>$document->file])}}" target="_blank">{{$document->name}}</a></td>
                        <td>{{$document->type()}}</td>
                        <td>{{$document->size()}}</td>
                        <td>@if($document->user_id != 0) <a href="{{route('profile',$document->user)}}" title="{{$document->user->name()}}"><img src="{{route('avatar',['q'=>$document->user->avatar])}}" class="blackboard-avatar blackboard-avatar-inline" alt="{{$document->user->name()}}"/></a> @else Uploaded by client @endif</td>
                        <td>{{$document->created_at->diffForHumans()}}</td>
                        <td class="last">
                            <a href="{{route('documents.edit',['document_id'=>$document,'client'=>$client->id,'process_id'=>$process_id,'step_id'=>$step["id"]])}}" class="btn btn-success btn-sm">Edit</a>
                            {{ Form::open(['id' => 'documentDelete','method' => 'DELETE','route' => ['documents.destroy','id'=>$document->id,'client_id' =>$document->client_id,'process_id'=>$process_id,'step_id'=>$step["id"]],'style'=>'display:inline']) }}
                            <a href="#" class="delete deleteDoc btn btn-danger btn-sm">Delete</a>
                            {{Form::close() }}
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="100%" class="text-center">No documents match those criteria.</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <style>
        a:focus{
            outline:none !important;
            border:0px !important;
        }

        .activity a{
            color: rgba(0,0,0,0.5) !important;
        }

        .activity a.dropdown-item {
            color:#212529 !important;
        }

        .btn-comment{
            padding: .25rem .25rem;
            font-size: .575rem;
            line-height: 1;
            border-radius: .2rem;
        }

        .modal-dialog {
            max-width: 700px;
            margin: 1.75rem auto;
            min-width: 500px;
        }

        .modal .chosen-container, .modal .chosen-container-multi{
            width:98% !important;
        }

        .chosen-container, .chosen-container-multi{
            line-height: 30px;
        }

        .modal-open .modal{
            padding-right: 0px !important;
        }

        .progress { position:relative; width:100%; border: 1px solid #7F98B2; padding: 1px; border-radius: 3px; display:none; }
        .bar { background-color: #B4F5B4; width:0%; height:25px; border-radius: 3px; }
        .percent { position:absolute; display:inline-block; top:3px; left:48%; color: #7F98B2;}
    </style>
@endsection