@extends('adminlte.default')

@section('title') Visits @endsection

@section('header')
    <div class="container-fluid container-title row">
        <div class="col-sm-1 pr-0"><h3>@yield('title')</h3></div>
    </div>
@endsection

@section('content')
    <div class="container-fluid">

    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-hover table-sm">
            <thead class="btn-dark">
            <tr>
                <th nowrap>@sortablelink('company', 'Name')</th>
                <th nowrap>@sortablelink('email', 'Email')</th>
                <th nowrap>@sortablelink('cell', 'Cellphone Number')</th>
                {{--<th nowrap>@sortablelink('section', 'Section')</th>--}}
                {{--<th nowrap>@sortablelink('created_at', 'Created Date ')</th>
                <th nowrap>@sortablelink('created_by', 'Created By')</th>--}}
            </tr>
            </thead>
            <tbody>
            @php $related_parties_count = 0; @endphp
            @forelse($clients as $client)
                @if(!isset($client["related_party_id"]))
                    <tr>
                        <td><a href="{{route('clients.show',[$client["id"],$client["process_id"],$client["step_id"]])}}">{{(isset($client["company"] ) && $client["company"] != ' ' ? $client["company"]  : 'Not Captured')}}</a></td>
                        <td>{{!is_null($client["email"]) ? $client["email"] : ''}}</td>
                        <td>{{!is_null($client["contact"]) ? $client["contact"] : ''}}</td>
                    </tr >
                @else
                    @if(request()->clientf != 'primary')
                    @php $related_parties_count++ @endphp
                    <tr class="bg-gray-light">
                        <td><a href="{{route('relatedparty.related_index',$client["client_id"])}}">{{(isset($client["company"]) && $client["company"] != '' ? $client["company"] : 'Not Captured')}}</a></td>
                        <td>{{!is_null($client["email"]) ? $client["email"] : ''}}</td>
                        <td>{{!is_null($client["contact"]) ? $client["contact"] : ''}}</td>
                        <td><a href="javascript:void(0)" class="btn btn-sm btn-danger" title="Submit an application for signatures"><i class="fas fa-paper-plane"></i> </a></td>
                    </tr>
                    @endif()

                @endif
            @empty
                <tr>
                    <td colspan="100%" class="text-center"><small class="text-muted">No clients match those criteria.</small></td>
                </tr>
            @endforelse
            </tbody>

        </table>
    </div>

    <small class="text-muted">Found <b>{{count($clients)}}</b> clients matching those criteria.</small>
    </div>

@endsection
@section('extra-css')
    <link rel="stylesheet" href="{{asset('chosen/chosen.min.css')}}">
    <style>

        .modal .chosen-container, .modal .chosen-container-multi{
            width:98% !important;
        }

        .chosen-container, .chosen-container-multi{
            line-height: 30px;
        }
    </style>
@endsection