@php //$theme = app('App\Theme')->whereUserId(auth()->id())->first(); @endphp
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{!! asset('storage/favicon.ico') !!}" type="image/x-icon"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>{{ env("APP_NAME") }}</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{!! asset('fontawesome/css/all.css') !!}">
    <!-- IonIcons -->
    <link rel="stylesheet" href="{!! asset('css/ionicons.min.css') !!}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{!! asset('adminlte/dist/css/adminlte.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/custom.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/blackboard.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/progress-wizard.min.css') !!}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{!! asset('css/jquery-ui.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/perfect-scrollbar.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/bootstrap/bootstrap-multiselect.css') !!}">
    <style>
        .ui-tooltip, .arrow:after {
            background: black !important;
            border: 0px solid transparent !important;
        }
        .ui-tooltip {
            padding: 5px 10px;
            color: white !important;
            font-size: 11px !important;
            font-weight:normal !important;
            border-radius: 5px;
            text-transform: uppercase;
            box-shadow: 0 0 7px black;
        }
        .arrow {
            width: 60px;
            height: 14px;
            overflow: hidden;
            position: absolute;
            left: 50%;
            margin-left: 0px;
            bottom: -10px;
        }
        .arrow.top {
            top: -16px;
            bottom: auto;
        }
        .arrow.left {
            left: 20%;
        }
        .arrow:after {
            content: "";
            position: absolute;
            left: 20px;
            top: -20px;
            width: 25px;
            height: 25px;
            box-shadow: 6px 5px 9px -9px black;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        .arrow.top:after {
            bottom: -20px;
            top: auto;
        }

        /* =Tooltip Style -------------------- */

        /* Tooltip Wrapper */
        .has-tooltip {
            position: relative;
        }
        .has-tooltip .tooltip2 {
            opacity: 0;
            visibility: hidden;
            -webkit-transition: visibility 0s ease 0.5s,opacity .3s ease-in;
            -moz-transition: visibility 0s ease 0.5s,opacity .3s ease-in;
            -o-transition: visibility 0s ease 0.5s,opacity .3s ease-in;
            transition: visibility 0s ease 0.5s,opacity .3s ease-in;
        }
        .has-tooltip:hover .tooltip2 {
            opacity: 1;
            visibility: visible;
        }

        /* Tooltip Body */
        .tooltip2 {
            background-color: #222;
            bottom: 130%;
            color: #fff;
            font-size: 14px;
            left: -100%;
            margin-left: 0px;
            padding: 6px;
            position: absolute;
            text-align: left;
            text-decoration: none;
            text-shadow: none;
            width:auto;
            min-width: 600px;
            max-width: 1000px;
            overflow: auto;
            z-index: 4;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            -o-border-radius: 3px;
            border-radius: 3px;
        }

        /* Tooltip Caret */
        .tooltip2:after {
            border-top: 5px solid #222;
            border-left: 4px solid transparent;
            border-right: 4px solid transparent;
            bottom: -5px;
            content: " ";
            font-size: 0px;
            left: 25px;
            line-height: 0%;
            margin-left: -4px;
            position: absolute;
            width: 0px;
            z-index: 1;
        }

        .tooltip2 ol,.tooltip2 ul,.tooltip2 li{
            text-align: left;
            /*margin: 0px;
            padding:0px;*/
        }

        .wrapper, body, html {
            min-height: 100%;
             overflow-x: unset;
        }
        @auth

        @else
            .content-wrapper, .main-footer, .main-header {
                margin-left: 0px;
            }
            .main-footer{
                 width:100% !important;
             }
        @endauth
        /*@media only screen and (max-width: 600px) {*/
        @if(\Request::is('clients/*/progress/*'))
            .main-wrapper {
                padding-bottom: 75px;
            }
        @else
            .main-wrapper {
                padding-bottom: 145px;
            }
        @endif
        /*}*/

    </style>
    @yield('extra-css')

</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to to the body tag
to get the desired effect
|---------------------------------------------------------|
|LAYOUT OPTIONS | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition sidebar-mini" style="height:100% !important;">
<div id="overlay" style="display:none;">
    <div class="spinner"></div>
    <br/>
    Loading...
</div>
<div id="app" class="wrapper">
    @if(auth()->check() && auth()->user()->trial == 1)
        <attooh-trial-notification></attooh-trial-notification>
        <p id="countdown" style="text-align: center; position: absolute;top: 0;left: 0;z-index: 99999;"></p>
    @endif

@include('adminlte.header')
@auth
@include('adminlte.sidebar')
@endauth
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper main-wrapper">
        @auth

            @else
            <a href="{{route('home')}}/?office={{(\session()->has('office_id') ? \session()->get('office_id') : (isset($_GET['office'])?$_GET['office']:1))}}"><img src="{{ asset('assets/'.env("APP_LOGO")) }}" style="margin: 0px auto; width:300px;padding-top:10px;display:block;" align="center"/></a>
        @endauth
        @yield('header')
        <div class="row flash_msg">
            @if(Session::has('flash_success'))
                <div class="alert alert-success alert-dismissible blackboard-alert">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    <strong>Success!</strong> {{Session::get('flash_success')}}
                </div>
                {{Session::forget('flash_success')}}
            @endif

            @if(Session::has('flash_info'))
                <div class="alert alert-info alert-dismissible blackboard-alert">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    <strong>Notice.</strong> {{Session::get('flash_info')}}
                </div>
            @endif

            @if(Session::has('flash_danger'))
                <div class="alert alert-danger alert-dismissible blackboard-alert">
                    <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                    <strong>Error!</strong> {{Session::get('flash_danger')}}
                </div>
            @endif
        </div>
        @yield('content')
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <footer class="main-footer" style="width: 100%;">
        <!-- To the right -->
        <img src="{!! asset('assets/logo-md.jpg') !!}" alt="Blackboard Logo" style="width:200px;opacity: .8;padding-bottom: 7px;"><br />
        <div class="float-right d-none d-sm-block-down">
            Anything you want
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; {{\Carbon\Carbon::parse(now())->format('Y')}} <a href="https://www.blackboardbs.com">www.blackboardbs.com</a>.</strong> All rights reserved.
    </footer>

    <!-- Bootsrap Modal -->

    <div class="modal fade" id="edit_email_template">
        <div class="modal-dialog" style="width:800px !important;max-width:800px;">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <h5 class="modal-title">View Email Template</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="box-body">
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="email_id" id="email_id" >
                            <input type="hidden" class="form-control" name="activity_id" id="activity_id" >
                        </div>
                        <div class="form-group mt-3">
                            {{Form::label('name', 'Name')}}
                            {{Form::text('name',null,['class'=>'form-control','placeholder'=>'Name','id'=>'email_title'])}}

                        </div>

                        <div class="form-group">
                            {{Form::label('Email Body')}}
                            {{ Form::textarea('email_content', null, ['class'=>'form-control my-editor','size' => '30x10','id'=>'email_content']) }}

                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-default btn-sm pull-left" data-dismiss="modal">Close</button>
                            <button type="button" onclick="saveEmailTemplate()" class="btn btn-sm btn-primary">Use Template</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="popup">
        <div class="modal-dialog" style="width:450px !important;max-width:450px;">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="form-group text-center">
                        {{Form::label('popup', 'Please bring your Blackboard account up to date.')}}

                    </div>
                    <div class="form-group text-center">
                        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Got it</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirmModal">
        <div class="modal-dialog" style="width:450px !important;max-width:450px;">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <h5 class="modal-title">Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="form-group text-center">
                        {{Form::label('popup', '',['id'=>'confirmMessage'])}}

                    </div>
                    <div class="form-group text-center">
                        <button type="button" class="btn btn-sm btn-default" id="confirmOk">Yes</button>
                        <button type="button" class="btn btn-sm btn-default" id="confirmCancel">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="confirmEmailModal">
        <div class="modal-dialog" style="width:550px !important;max-width:550px;">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <h5 class="modal-title">Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="form-group text-left">
                        <input type="hidden" name="client_id" id="confirmEmailClient">
                        <label id='confirmEmailMessage'></label>

                    </div>
                    <div class="form-group text-left all-emails">
                        <ul id='confirmEmails'>

                        </ul>
                    </div>
                    <div class="form-group text-left input-group">
                        {{Form::text('extra-email', '',['class'=>'confirmExtraEmail form-control form-control-sm','size'=>'50','placeholder'=>'Enter email address'])}}
                        <div class="input-group-append-">
                            <a href="javascript:void(0)" class="btn btn-sm btn-secondary add-email">Add</a>
                        </div>

                    </div>
                    <div class="form-group text-center">
                        <button type="button" class="btn btn-sm btn-default" id="confirmEmailOk">Yes</button>
                        <button type="button" class="btn btn-sm btn-default" id="confirmEmailCancel">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="notifyModal">
        <div class="modal-dialog" style="width:450px !important;max-width:450px;">
            <div class="modal-content">
                <div class="modal-header text-center" style="border-bottom: 0px;padding:.5rem;">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body mx-3">
                    <div class="form-group text-center">
                        {{Form::label('popup', '',['id'=>'notifyMessage'])}}

                    </div>
                    <div class="form-group text-center">
                        <button type="button" class="btn btn-sm btn-default" id="notifyOk">Ok</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<script src="{!! asset('js/jquery/jquery.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/jquery/jquery-ui.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/popper.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/bootstrap/bootstrap.min.js') !!}" type="text/javascript"></script>
<!-- AdminLTE -->
<script src="{!! asset('adminlte/dist/js/adminlte.js') !!}" type="text/javascript"></script>
<script src="{!! asset('adminlte/dist/js/jscolor.js') !!}" type="text/javascript"></script>
<!-- OPTIONAL SCRIPTS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" rel="stylesheet"/>

<script src="{!! asset('js/moment.min.js') !!}" type="text/javascript"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=361nrfmxzoobhsuqvaj3hyc2zmknskzl4ysnhn78pjosbik2"></script>
<script src="{!! asset('js/tinymce/vue-tinymce.js') !!}" type="text/javascript"></script>
{{--@auth--}}
<script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
<!-- SortableJS -->
<script src="https://unpkg.com/sortablejs@1.4.2"></script>
<!-- VueSortable -->
<script src="https://unpkg.com/vue-sortable@0.1.3"></script>
{{--@endauth--}}
<link href="{!! asset('css/qrscanner.css') !!}" rel="stylesheet">
<script src="{!! asset('js/qrscanner.js') !!}"></script>

<script>
    $(document).ready(function(){


        var active_elem = $(".nav-sidebar").find(".active");

        $(active_elem).parent('li').parent('ul').parent('li').addClass('menu-open');
        //$(active_elem).parent('li').parent('ul').addClass('test');

    });
</script>
<script src="{!! asset('adminlte/dist/js/custom.js') !!}" type="text/javascript"></script>
<script src="{!! asset('chosen/chosen.jquery.min.js') !!}" type="text/javascript"></script>
<script src="{!! asset('chosen/docsupport/init.js') !!}" type="text/javascript" charset="utf-8"></script>
<script src="{!! asset('js/autocomplete.js') !!}" type="text/javascript"></script>
<script src="{!! asset('js/bootstrap/bootstrap-multiselect.min.js') !!}" type="text/javascript"></script>
<script>
    $(".delete").on("click", function(e){
        e.preventDefault();
        if (!confirm("Are you sure you want to delete this record?")){
            return false;
        } else {
            $(this).parents('form:first').submit();
        }

    });

    $(document).ready(function()
    {
        $('#admin-menu').on('click',function(e){
            //alert();
            $(".nav-sidebar").find('li.admin-menu').toggle();
        })

        $(document).ajaxStart(function() {
            $('#overlay').fadeIn();
            })
        $(document).ajaxStop(function() {
            $('#overlay').fadeOut();
            });
    });
</script>
<script src="{!! asset('js/perfect-scrollbar.js') !!}" type="text/javascript"></script>
<script>

    $(function(){

            $('[data-toggle="tooltip"]').tooltip({
                items: "[data-original-title]",
                content: function() {
                    var element = $( this );
                    if ( element.is( "[data-original-title]" ) ) {
                        return element.attr( "title" );
                    }
                },
                position: {
                    my: "center bottom-10", // the "anchor point" in the tooltip element
                    at: "center top", // the position of that anchor point relative to selected element
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                    }
                }
            })

            $('.chosen-select').chosen();
            $('.chosen-select').css('width', '80%');

            $(".step-dropdown").change(function(){

                 var url = $('option:selected',this).data('path');
                 window.location.href = url;
            });


    })

    $("#viewprocess").on('change', function () {
        let client_id = $('#client_id').val();
        let process_id = $('#viewprocess').val();
        let step_id = 0;

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "GET",
        url: '/clients/getfirststep/' + client_id + '/' + process_id,
        success: function( data ) {
            window.location.href = '/clients/' + client_id + '/progress/' + process_id + '/' + data;
        }
    });
})

@if(auth()->id() == '1000')
$(document).ready(function() {

    if(localStorage.getItem('popState') != 'shown'){
        $("#popup").delay(2000).modal('show');
        localStorage.setItem('popState','shown')
    }

});
@endif
</script>
<script>
    function validateEmail(sEmail) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(sEmail)) {
            return false;
        }else{
            return true;
        }
    }

    $(function(){
        $('.add-email').on('click',function(){
            let email = $('#confirmEmailModal').find('.confirmExtraEmail').val();
            if(validateEmail(email)) {
                $('#confirmEmailModal').find('.confirmExtraEmail').removeClass('is-invalid');
                $('#confirmEmailModal').find('#confirmEmails').append('<li>'+email+'</li>');
                $('#confirmEmailModal').find('.all-emails').append('<input type="hidden" name="extra-emails[]" value="'+email+'">');
                $('#confirmEmailModal').find('.confirmExtraEmail').val('');
            } else {
                $('#confirmEmailModal').find('.confirmExtraEmail').addClass('is-invalid');
            }
        })
    });

    function confirmDialog(message, onConfirm){
        var fClose = function(){
            modal.modal("hide");
        };

        var fClose2 = function(){
            modal.modal("hide");
            if ($(document).find('#modalSendTemplate').hasClass('show')) {
                $('#modalSendTemplate').find('#sendtemplatecomposeemailsend').attr("disabled", false);
                $('#modalSendTemplate').find('.sendtemplatecancel').attr("disabled", false);
                $('#modalSendTemplate').find('#sendcomposemessage').html('');
                $('#modalSendTemplate').find('#sendtemplatetemplateemailsend').attr("disabled", false);
                $('#modalSendTemplate').modal('hide');
            }
            if ($(document).find('#modalSendDocument').hasClass('show')) {
                $('#modalSendDocument').find('#senddocumentcomposeemailsend').attr("disabled", false);
                $('#modalSendDocument').find('.senddocumentcancel').attr("disabled", false);
                $('#modalSendDocument').find('#sendcomposemessaged').html('');
                $('#modalSendDocument').find('#senddocumenttemplateemailsend').attr("disabled", false);
                $('#modalSendDocument').modal('hide');
            }
            if ($(document).find('#modalSendMA').hasClass('show')) {
                $('#modalSendMA').find('#sendmatemplateemailsend').attr("disabled", true);
                $('#modalSendMA').find('.sendmacancel').attr("disabled", true);
                $('#modalSendMA').find('#sendmamessage').html('');
                $('#modalSendMA').find('#sendmacomposeemailsend').attr("disabled", false);
                $('#modalSendMA').modal('hide');
            }
        };

        var modal = $("#confirmModal");
        modal.modal("show");
        $("#confirmMessage").empty().append(message);
        $("#confirmOk").unbind().one('click', onConfirm).one('click', fClose);
        $("#confirmCancel").unbind().one("click", fClose2);
    }

    function confirmEmailDialog(message, client_id, email, onConfirm){
        var fClose = function(){
            modal.modal("hide");
        };

        var modal = $("#confirmEmailModal");
        modal.modal("show");

        if(email.length > 0){
            $("#confirmEmails").empty().append('<li>'+email+'</li>');
        }

        $('.all-emails').append('<input type="hidden" name="extra-emails[]" value="'+email+'" />');
        $("#confirmEmailClient").val(client_id);
        $("#confirmEmailMessage").empty().append(message);

        $("#confirmEmailOk").unbind().one('click', onConfirm).one('click', fClose);
        $("#confirmEmailCancel").unbind().one("click", fClose);
    }

    function notifyDialog(message){
        var fClose = function(){
            modal.modal("hide");
        };

        var modal = $("#notifyModal");
        modal.modal("show");
        $("#notifyMessage").empty().append(message);
        $("#notifyOk").unbind().one('click', fClose);
    }

    function sendClientEmail(client_id, client_email) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let YOUR_MESSAGE_STRING_CONST;

        if(client_email.length > 0) {
            YOUR_MESSAGE_STRING_CONST = "Are you sure you want to send an email to the following recipients?";
        } else {
            YOUR_MESSAGE_STRING_CONST = "";
        }


        confirmEmailDialog(YOUR_MESSAGE_STRING_CONST, client_id, client_email, function () {
            var emails = $('input[name="extra-emails[]"]').map(function(){
                return this.value;
            }).get();

            var process_id = {{(isset($process_id)?$process_id:'1')}};
            var step_id = {{(isset($step['id'])?$step['id']:'')}};

            $.ajax({
                type: "POST",
                url: '/client/' + client_id +'/sendclientemail',
                data: {client_id: client_id,emails:emails,process_id:process_id,step_id:step_id},
                success: function (data) {
                    if (data.message === 'Success') {
                        $('.flash_msg').html('<div class="alert alert-success">Email successfully sent</div>');
                    } else {
                        $('.flash_msg').html('<div class="alert alert-danger">An error occured while trying to send the email.</div>');
                    }
                    $('.all-emails').empty().append('<ul id=\'confirmEmails\'>\n' +
                        '\n' +
                        '                        </ul>');
                    /*setTimeout(function () {
                        window.location.reload();
                    }, 2000);*/
                }
            });
        });
    }

    (function($) {

        $(".cata-sub-nav").on('scroll', function() {
            $val = $(this).scrollLeft();

            if($(this).scrollLeft() + $(this).innerWidth()>=$(this)[0].scrollWidth){
                $(".nav-next").hide();
            } else {
                $(".nav-next").show();
            }

            if($val == 0){
                $(".nav-prev").hide();
            } else {
                $(".nav-prev").show();
            }
        });
        var w = $('.sidebar').outerWidth(true);
        console.log( 'init-scroll: ' + $(".nav-next").scrollLeft() );
        $(".nav-next").on("click", function(){

            $(".cata-sub-nav").animate( { scrollLeft: '+=460' }, 200);
        });
        $(".nav-prev").on("click", function(){
            $(".cata-sub-nav").animate( { scrollLeft: '-=460' }, 200);
        });



    })(jQuery);
</script>

@yield('extra-js')
<script>
    $(function() {

        var numItems = $('.progress-indicator').find('.card').length;
        if (numItems < parseInt(6)) {
            $('.progress-indicator').addClass('justify-content-center');
        }
        if (numItems === parseInt(6)) {
            $('.progress-indicator').addClass('justify-content-center');
        }
        if (numItems > parseInt(6)) {
            let offs = $('#' + $('#active_step_id').val()).offset().left;
            $('#scrolling-wrapper').animate({scrollLeft: offs - 300}, 0);
        }
    })
</script>
</body>
</html>
