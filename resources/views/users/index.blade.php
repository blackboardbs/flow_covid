@extends('adminlte.default')

@section('title') Users @endsection

@section('header')
    <div class="container-fluid container-title">
        <h3>@yield('title')</h3>
        <a href="{{route('users.create')}}" class="btn btn-dark btn-sm float-right"><i class="fa fa-plus"></i> User</a>
    </div>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="container">
    <form class="form-inline mt-3">
        Show &nbsp;
        {{Form::select('s',['all'=>'All','5'=>'5','10'=>'10','15' => '15','20' => '20','25'=>'25','30'=>'30'],old('selection'),['class'=>'form-control form-control-sm'])}}
        &nbsp; matching &nbsp;
        <div class="input-group input-group-sm">
            <div class="input-group-prepend">
                <div class="input-group-text">
                    <i class="fa fa-search"></i>
                </div>
            </div>
            {{Form::text('q',old('qy'),['class'=>'form-control form-control-sm','placeholder'=>'Search...'])}}
        </div>
            &nbsp; between &nbsp;
            {{Form::date('f',old('f'),['class'=>'form-control form-control-sm'])}}
            &nbsp; and &nbsp;
            {{Form::date('t',old('t'),['class'=>'form-control form-control-sm'])}}
            <button type="submit" class="btn btn-sm btn-secondary ml-2 mr-2"><i class="fa fa-search"></i> Search</button>
            <a href="{{route('users.index')}}" class="btn btn-sm btn-info"><i class="fa fa-eraser"></i> Clear</a>
        </div>
    </form>

    <hr>

    <div class="table-responsive">
        <table class="table table-bordered table-sm table-hover">
            <thead class="btn-dark">
            <tr>
                <th></th>
                <th>Name</th>
                <th>Email</th>
                <th><abbr title="This is the amount of locations a user is assigned to.">Domain</abbr></th>
                <th><abbr title="This is the amount of roles a user is assigned to.">Roles</abbr></th>
                <th>Added</th>
                <th>Last Login</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @forelse($users as $user)
                <tr>
                    <td><img src="{{route('avatar',['q'=>$user->avatar])}}" class="blackboard-avatar blackboard-avatar-inline"/></td>
                    <td><a href="{{route('profile',$user)}}">{{$user->name()}}</a></td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->domainCount()}}</td>
                    <td>
                        @foreach($user->roles as $roles)
                            {{$roles->display_name}} <br/>
                        @endforeach
                    </td>
                    <td>{{$user->created_at->diffForHumans()}}</td>
                    <td>{{$user->last_login_at}}</td>
                    <td class="last">
                            <a href="{{route('users.edit',$user)}}" class="btn btn-success btn-sm">Edit</a>
                        @if(auth()->id() != $user->id)
                            &nbsp;
                        @if($user->deleted_at != null)
                            <a class="reactivate btn btn-outline-danger btn-sm" href="{{route('users.activateuser',$user)}}">Activate</a>
                        @else
                            <a class="deactivate btn btn-danger btn-sm" href="{{route('users.deactivate',$user)}}">Deactivate</a>
                        @endif
                        @endif
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="100%" class="text-center">No clients match those criteria.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
    </div>
    </div>
@endsection

@section('extra-js')
    <script>
        $(".deactivate").click(function (e) {
            e.preventDefault();
            var conf = confirm("Are you sure you want to deactivate this user account?");
            if(conf)
                window.location = $(this).attr("href");
        });
        $(".reactivate").click(function (e) {
            e.preventDefault();
            var conf = confirm("Are you sure you want to reactivate this user account?");
            if(conf)
                window.location = $(this).attr("href");
        });
    </script>
@endsection