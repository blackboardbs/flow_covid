<?php

namespace App\Mail;

use App\Client;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegistrationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $user;
    public $office;

    public function __construct(User $user,$office,$password)
    {
        $this->user = $user;
        $this->office = $office;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Covid-19 Screening App Registration')->view('emails.registration_mail')->with(['password'=>$this->password,'user'=>$this->user])->attach(public_path('qrcodes/'.$this->office->id.'/qrcode.png'));
    }
}
