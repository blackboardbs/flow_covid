<?php

namespace App\Http\Controllers;

use App\ActionableAmount;
use App\ActionableBoolean;
use App\ActionableContent;
use App\ActionableDate;
use App\ActionableDocument;
use App\ActionableDocumentEmail;
use App\ActionableDropdown;
use App\ActionableDropdownItem;
use App\ActionableHeading;
use App\ActionableImageUpload;
use App\ActionableInteger;
use App\ActionableMultipleAttachment;
use App\ActionableNotification;
use App\ActionablePercentage;
use App\ActionableSubheading;
use App\ActionableTemplateEmail;
use App\ActionableText;
use App\ActionableTextarea;
use App\ActionableVideoUpload;
use App\ActionableVideoYoutube;
use App\Activity;
use App\Area;
use App\AreaUser;
use App\Division;
use App\DivisionUser;
use App\Mail\RegistrationMail;
use App\Office;
use App\OfficeUser;
use App\Process;
use App\ProcessGroup;
use App\Region;
use App\RegionUser;
use App\RoleUser;
use App\Step;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class HelpportalAPIController extends Controller
{
    public function createTrial(Request $request){

        $password = str_random(8);

        $user = new User();
        $user->first_name = $request->input('name');
        $user->last_name = $request->input('surname');
        $user->email = $request->input('email');
        $user->verified = '1';
        $user->password = bcrypt($password);
        $user->save();

        User::where('id',$user->id)->update([
            'hash_first_name' => DB::raw("AES_ENCRYPT('".$request->input('first_name')."','Qwfe345dgfdg')"),
            'hash_last_name' => DB::raw("AES_ENCRYPT('".$request->input('last_name')."','Qwfe345dgfdg')")
        ]);

        RoleUser::insert(['user_id'=>$user->id,'role_id'=>'1']);

        $division = Division::where('name',$request->input('country'))->first();

        if($division) {
            $division_id = $division->id;
        } else {
            $ndivision = new Division();
            $ndivision->name = $request->input('country');
            $ndivision->save();

            $division_id = $ndivision->id;
        }

        $division_user = DivisionUser::insert(['user_id'=>$user->id,'division_id'=>$division_id]);

        $region = Region::where('name',$request->input('state'))->first();

        if($region) {
            $region_id = $region->id;
        } else {
            $nregion = new Region();
            $nregion->name = $request->input('state');
            $nregion->division_id = $division_id;
            $nregion->save();

            $region_id = $nregion->id;
        }

        $region_user = RegionUser::insert(['user_id'=>$user->id,'region_id'=>$region_id]);

        $area = Area::where('name',$request->input('city'))->first();

        if($area) {
            $area_id = $area->id;
        } else {
            $narea = new Area();
            $narea->name = $request->input('city');
            $narea->region_id = $region_id;
            $narea->save();

            $area_id = $narea->id;
        }

        $area_user = AreaUser::insert(['user_id'=>$user->id,'area_id'=>$area_id]);

        $office = new Office;
        $office->name = $request->input('company_name');
        $office->area_id = $area_id;
        $office->save();

        $office_user = OfficeUser::insert(['user_id'=>$user->id,'office_id'=>$office->id]);

        $process_group = new ProcessGroup();
        $process_group->name = 'Covid19 Assessment';
        $process_group->office_id = $office->id;
        $process_group->save();

        $process = new Process();
        $process->name = 'Visitor Process';
        $process->not_started_colour = 'rgba(242,99,91,0.7)';
        $process->started_colour = 'rgba(252,182,61,0.7)';
        $process->completed_colour = 'rgba(50,193,75,0.7)';
        $process->office_id = $office->id;
        $process->process_type_id = 1;
        $process->process_group_id = $process_group->id;
        $process->docfusion_process_id = null;
        $process->docfusion_template_id = null;
        $process->save();

        $step = new Step();
        $step->name = 'Capture Visit';
        $step->order = '1';
        $step->process_id = $process->id;
        $step->colour = 'rgba(242,99,91,0.7)';
        $step->group = '1';
        $step->signature = '0';
        $step->save();

        $activities = $this->defaultActivities();

        foreach ($activities as $activity_key => $activity_input) {
            $activity = new Activity();

            $actionable = $this->createActionable($activity_input['type']);

            $activity->name = $activity_input['name'];
            $activity->order = $activity_key + 1;
            $activity->actionable_id = (isset($actionable->id) ? $actionable->id : 0);
            $activity->actionable_type = $this->getActionableType($activity_input['type']);
            $activity->kpi = (isset($activity_input['kpi']) && $activity_input['kpi'] == "on") ? 1 : null;
            $activity->comment = (isset($activity_input['comment']) && $activity_input['comment'] == "on") ? 1 : null;
            $activity->value = (isset($activity_input['value']) && $activity_input['value'] == "on") ? 1 : null;
            $activity->procedure = (isset($activity_input['procedure']) && $activity_input['procedure'] == "on") ? 1 : null;
            $activity->client_activity = (isset($activity_input['client']) && $activity_input['client'] == "on") ? 1 : null;
            $activity->step_id = $step->id;
            $activity->threshold = (isset($activity_input['threshold']) ? $this->getThresholdAsSeconds($activity_input['threshold_time'], $activity_input['threshold_type']) : 0);
            $activity->weight = (isset($activity_input['weight']) ? $activity_input['weight'] : 0);
            $activity->tooltip = (isset($activity_input['tooltip']) ? $activity_input['tooltip'] : '');
            $activity->show_tooltip = (isset($activity_input['show_tooltip']) && $activity_input['show_tooltip'] == "on") ? 1 : 0;
            //todo add dependant activities
            $activity->user_id = (!isset($activity_input['user']) || $activity_input['user'] == 0) ? null : $activity_input['user'];
            $activity->grouped = (isset($activity_input['grouping']) && $activity_input['grouping'] == "on") ? 1 : 0;
            $activity->grouping = (isset($activity_input['grouping_value'])) ? $activity_input['grouping_value'] : null;
            $activity->report = (isset($activity_input['report']) && $activity_input['report'] == "on") ? 1 : 0;
            if ($activity_input['type'] == 'heading' || $activity_input['type'] == 'subheading') {
                $activity->client_bucket = 1;
            } else {
                $activity->client_bucket = (isset($activity_input['client_bucket']) && $activity_input['client_bucket'] == "on") ? 1 : 0;
            }
            $activity->future_date = (isset($activity_input['future_date']) && $activity_input['future_date'] == "on") ? 1 : 0;
            $activity->position = (isset($activity_input['position']) ? $activity_input['position'] : 0);
            $activity->level = (isset($activity_input['level']) ? $activity_input['level'] : 0);
            $activity->color = (isset($activity_input['color']) && $activity_input['color'] != '#hsla(0,0%,0%,0)' ? $activity_input['color'] : null);
            $activity->text_content = (isset($activity_input['text_content']) && $activity_input['text_content'] != '' ? $activity_input['text_content'] : null);
            $activity->save();

            //if activity is a dropdown type
            if ($activity_input['type'] == 'dropdown') {

                //only add dropdown items if there is input
                if (isset($activity_input['dropdown_items'])) {

                    //loop over each dropdown item
                    foreach ($activity_input['dropdown_items'] as $dropdown_item) {
                        $actionable_dropdown_item = new ActionableDropdownItem;
                        $actionable_dropdown_item->actionable_dropdown_id = $actionable->id;
                        $actionable_dropdown_item->name = $dropdown_item;
                        $actionable_dropdown_item->save();
                    }
                }
            }
        }

        if(!file_exists(public_path('qrcodes/'.$office->id))){
            File::makeDirectory(public_path('qrcodes/'.$office->id),0777,true);
        }
        $img = QrCode::format('png')->size(400)->generate(env("APP_URL").'/?office='.$office->id, public_path('qrcodes/'.$office->id.'/qrcode.png'));

        Mail::to($request->input('email'))->send(new RegistrationMail($user,$office,$password));

        return true;
    }

    public function defaultActivities(){

        $activities = array();

        array_push($activities,[
            "name"=> "Current Temperature (To be completed at reception):",
            "type"=> "text",
            "level"=> "0",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "0",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Do you presently have any of the following symptoms? (Tick all that apply)",
            "type"=> "dropdown",
            "level"=> "0",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "0",
            "dropdown_items"=> [
                '0'=> "Over above 38°C",
                '1'=> "History of fever within past 14 days",
                '2'=> "Cough",
                '3'=> "Chills (feeling cold from flu)",
                '4'=> "Body pains (flu related)",
                '5'=> "Sore throat",
                '6'=> "Shortness of breath (acute)",
                '7'=> "Nausea/vomiting",
                '8'=> "Diarrhoea",
                '9'=> "General weakness",
                '10'=> "Irritability/confusion",
                '11'=> "None of the above",
            ],
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "type"=> "content",
            "name"=> "Guest/Children",
            "text_content"=> NULL,
            "kpi"=> "on",
            "level"=> "0",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "1",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Name",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "1",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Current Temperature:",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "1",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Do you presently have any of the following symptoms? (Tick all that apply)",
            "type"=> "dropdown",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "1",
            "dropdown_items"=>[
                '0'=> "Over above 38°C",
                '1'=> "History of fever within past 14 days",
                '2'=> "Cough",
                '3'=> "Chills (feeling cold from flu)",
                '4'=> "Body pains (flu related)",
                '5'=> "Sore throat",
                '6'=> "Shortness of breath (acute)",
                '7'=> "Nausea/vomiting",
                '8'=> "Diarrhoea",
                '9'=> "General weakness",
                '10'=> "Irritability/confusion",
                '11'=> "None of the above",
            ],
            "tooltip"=> NULL,
            "styleid"=>[
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "type"=> "content",
            "name"=> "Guest/Children",
            "text_content"=> NULL,
            "kpi"=> "on",
            "level"=> "0",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "2",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Name",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "2",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Current Temperature:",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "2",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Do you presently have any of the following symptoms? (Tick all that apply)",
            "type"=> "dropdown",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "2",
            "dropdown_items"=>[
                '0'=> "Over above 38°C",
                '1'=> "History of fever within past 14 days",
                '2'=> "Cough",
                '3'=> "Chills (feeling cold from flu)",
                '4'=> "Body pains (flu related)",
                '5'=> "Sore throat",
                '6'=> "Shortness of breath (acute)",
                '7'=> "Nausea/vomiting",
                '8'=> "Diarrhoea",
                '9'=> "General weakness",
                '10'=> "Irritability/confusion",
                '11'=> "None of the above",
            ],
            "tooltip"=> NULL,
            "styleid"=>[
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "type"=> "content",
            "name"=> "Guest/Children",
            "text_content"=> NULL,
            "kpi"=> "on",
            "level"=> "0",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "3",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Name",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "3",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Current Temperature:",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "3",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Do you presently have any of the following symptoms? (Tick all that apply)",
            "type"=> "dropdown",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "3",
            "dropdown_items"=>[
                '0'=> "Over above 38°C",
                '1'=> "History of fever within past 14 days",
                '2'=> "Cough",
                '3'=> "Chills (feeling cold from flu)",
                '4'=> "Body pains (flu related)",
                '5'=> "Sore throat",
                '6'=> "Shortness of breath (acute)",
                '7'=> "Nausea/vomiting",
                '8'=> "Diarrhoea",
                '9'=> "General weakness",
                '10'=> "Irritability/confusion",
                '11'=> "None of the above",
            ],
            "tooltip"=> NULL,
            "styleid"=>[
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "type"=> "content",
            "name"=> "Guest/Children",
            "text_content"=> NULL,
            "kpi"=> "on",
            "level"=> "0",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "4",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Name",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "4",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Current Temperature:",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "4",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Do you presently have any of the following symptoms? (Tick all that apply)",
            "type"=> "dropdown",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "4",
            "dropdown_items"=>[
                '0'=> "Over above 38°C",
                '1'=> "History of fever within past 14 days",
                '2'=> "Cough",
                '3'=> "Chills (feeling cold from flu)",
                '4'=> "Body pains (flu related)",
                '5'=> "Sore throat",
                '6'=> "Shortness of breath (acute)",
                '7'=> "Nausea/vomiting",
                '8'=> "Diarrhoea",
                '9'=> "General weakness",
                '10'=> "Irritability/confusion",
                '11'=> "None of the above",
            ],
            "tooltip"=> NULL,
            "styleid"=>[
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "type"=> "content",
            "name"=> "Guest/Children",
            "text_content"=> NULL,
            "kpi"=> "on",
            "level"=> "0",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "5",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Name",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "5",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Current Temperature:",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "5",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Do you presently have any of the following symptoms? (Tick all that apply)",
            "type"=> "dropdown",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "5",
            "dropdown_items"=>[
                '0'=> "Over above 38°C",
                '1'=> "History of fever within past 14 days",
                '2'=> "Cough",
                '3'=> "Chills (feeling cold from flu)",
                '4'=> "Body pains (flu related)",
                '5'=> "Sore throat",
                '6'=> "Shortness of breath (acute)",
                '7'=> "Nausea/vomiting",
                '8'=> "Diarrhoea",
                '9'=> "General weakness",
                '10'=> "Irritability/confusion",
                '11'=> "None of the above",
            ],
            "tooltip"=> NULL,
            "styleid"=>[
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "type"=> "content",
            "name"=> "Guest/Children",
            "text_content"=> NULL,
            "kpi"=> "on",
            "level"=> "0",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "6",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Name",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "6",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Current Temperature:",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "6",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Do you presently have any of the following symptoms? (Tick all that apply)",
            "type"=> "dropdown",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "6",
            "dropdown_items"=>[
                '0'=> "Over above 38°C",
                '1'=> "History of fever within past 14 days",
                '2'=> "Cough",
                '3'=> "Chills (feeling cold from flu)",
                '4'=> "Body pains (flu related)",
                '5'=> "Sore throat",
                '6'=> "Shortness of breath (acute)",
                '7'=> "Nausea/vomiting",
                '8'=> "Diarrhoea",
                '9'=> "General weakness",
                '10'=> "Irritability/confusion",
                '11'=> "None of the above",
            ],
            "tooltip"=> NULL,
            "styleid"=>[
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "type"=> "content",
            "name"=> "Guest/Children",
            "text_content"=> NULL,
            "kpi"=> "on",
            "level"=> "0",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "7",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Name",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "7",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Current Temperature:",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "7",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Do you presently have any of the following symptoms? (Tick all that apply)",
            "type"=> "dropdown",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "7",
            "dropdown_items"=>[
                '0'=> "Over above 38°C",
                '1'=> "History of fever within past 14 days",
                '2'=> "Cough",
                '3'=> "Chills (feeling cold from flu)",
                '4'=> "Body pains (flu related)",
                '5'=> "Sore throat",
                '6'=> "Shortness of breath (acute)",
                '7'=> "Nausea/vomiting",
                '8'=> "Diarrhoea",
                '9'=> "General weakness",
                '10'=> "Irritability/confusion",
                '11'=> "None of the above",
            ],
            "tooltip"=> NULL,
            "styleid"=>[
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "type"=> "content",
            "name"=> "Guest/Children",
            "text_content"=> NULL,
            "kpi"=> "on",
            "level"=> "0",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "8",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Name",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "8",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Current Temperature:",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "8",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Do you presently have any of the following symptoms? (Tick all that apply)",
            "type"=> "dropdown",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "8",
            "dropdown_items"=>[
                '0'=> "Over above 38°C",
                '1'=> "History of fever within past 14 days",
                '2'=> "Cough",
                '3'=> "Chills (feeling cold from flu)",
                '4'=> "Body pains (flu related)",
                '5'=> "Sore throat",
                '6'=> "Shortness of breath (acute)",
                '7'=> "Nausea/vomiting",
                '8'=> "Diarrhoea",
                '9'=> "General weakness",
                '10'=> "Irritability/confusion",
                '11'=> "None of the above",
            ],
            "tooltip"=> NULL,
            "styleid"=>[
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "type"=> "content",
            "name"=> "Guest/Children",
            "text_content"=> NULL,
            "kpi"=> "on",
            "level"=> "0",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "9",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Name",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "9",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Current Temperature:",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "9",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Do you presently have any of the following symptoms? (Tick all that apply)",
            "type"=> "dropdown",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "9",
            "dropdown_items"=>[
                '0'=> "Over above 38°C",
                '1'=> "History of fever within past 14 days",
                '2'=> "Cough",
                '3'=> "Chills (feeling cold from flu)",
                '4'=> "Body pains (flu related)",
                '5'=> "Sore throat",
                '6'=> "Shortness of breath (acute)",
                '7'=> "Nausea/vomiting",
                '8'=> "Diarrhoea",
                '9'=> "General weakness",
                '10'=> "Irritability/confusion",
                '11'=> "None of the above",
            ],
            "tooltip"=> NULL,
            "styleid"=>[
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "type"=> "content",
            "name"=> "Guest/Children",
            "text_content"=> NULL,
            "kpi"=> "on",
            "level"=> "0",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "10",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Name",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "10",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Current Temperature:",
            "type"=> "text",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "10",
            "tooltip"=> NULL,
            "styleid"=> [
                '0'=>
                    NULL
            ]
        ]);
        array_push($activities,[
            "name"=> "Do you presently have any of the following symptoms? (Tick all that apply)",
            "type"=> "dropdown",
            "kpi"=> "on",
            "level"=> "1",
            "position"=> "0",
            "color"=> "hsl(0,0%,0%)",
            "grouping_value"=> "10",
            "dropdown_items"=>[
                '0'=> "Over above 38°C",
                '1'=> "History of fever within past 14 days",
                '2'=> "Cough",
                '3'=> "Chills (feeling cold from flu)",
                '4'=> "Body pains (flu related)",
                '5'=> "Sore throat",
                '6'=> "Shortness of breath (acute)",
                '7'=> "Nausea/vomiting",
                '8'=> "Diarrhoea",
                '9'=> "General weakness",
                '10'=> "Irritability/confusion",
                '11'=> "None of the above",
            ],
            "tooltip"=> NULL,
            "styleid"=>[
                '0'=>
                    NULL
            ]
        ]);

        return $activities;
    }

    public function getActionableType($type)
    {
        //activity type hook
        switch ($type) {
            case 'heading':
                return 'App\ActionableHeading';
                break;
            case 'subheading':
                return 'App\ActionableSubheading';
                break;
            case 'content':
                return 'App\ActionableContent';
                break;
            case 'text':
                return 'App\ActionableText';
                break;
            case 'textarea':
                return 'App\ActionableTextarea';
                break;
            case 'percentage':
                return 'App\ActionablePercentage';
                break;
            case 'integer':
                return 'App\ActionableInteger';
                break;
            case 'amount':
                return 'App\ActionableAmount';
                break;
            case 'videoyoutube':
                return 'App\ActionableVideoYoutube';
                break;
            case 'videoupload':
                return 'App\ActionableVideoUpload';
                break;
            case 'imageupload':
                return 'App\ActionableImageUpload';
                break;
            case 'template_email':
                return 'App\ActionableTemplateEmail';
                break;
            case 'document_email':
                return 'App\ActionableDocumentEmail';
                break;
            case 'document':
                return 'App\ActionableDocument';
                break;
            case 'dropdown':
                return 'App\ActionableDropdown';
                break;
            case 'date':
                return 'App\ActionableDate';
                break;
            case 'boolean':
                return 'App\ActionableBoolean';
                break;
            case 'notification':
                return 'App\ActionableNotification';
                break;
            case 'multiple_attachment':
                return 'App\ActionableMultipleAttachment';
            default:
                abort(500, 'Error');
                break;
        }
    }

    public function getThresholdAsSeconds($time, $type)
    {
        switch ($type) {
            case 'seconds':
                return $time;
                break;
            case 'minutes':
                return $time * 60;
                break;
            case 'hours':
                return $time * 60 * 60;
                break;
            case 'days':
                return $time * 60 * 60 * 24;
                break;
            default:
                return $time;
                break;
        }
    }

    public function createActionable($type)
    {
        //activity type hook
        switch ($type) {
            case 'heading':
                return ActionableHeading::create();
                break;
            case 'subheading':
                return ActionableSubheading::create();
                break;
            case 'content':
                return ActionableContent::create();
                break;
            case 'text':
                return ActionableText::create();
                break;
            case 'textarea':
                return ActionableTextarea::create();
                break;
            case 'percentage':
                return ActionablePercentage::create();
                break;
            case 'integer':
                return ActionableInteger::create();
                break;
            case 'amount':
                return ActionableAmount::create();
                break;
            case 'videoupload':
                return ActionableVideoUpload::create();
                break;
            case 'imageupload':
                return ActionableImageUpload::create();
                break;
            case 'videoyoutube':
                return ActionableVideoYoutube::create();
                break;
            case 'template_email':
                return ActionableTemplateEmail::create();
                break;
            case 'document_email':
                return ActionableDocumentEmail::create();
                break;
            case 'document':
                return ActionableDocument::create();
                break;
            case 'dropdown':
                return ActionableDropdown::create();
                break;
            case 'date':
                return ActionableDate::create();
                break;
            case 'boolean':
                return ActionableBoolean::create();
                break;
            case 'notification':
                return ActionableNotification::create();
                break;
            case 'multiple_attachment':
                return ActionableMultipleAttachment::create();
            default:
                abort(500, 'Error');
                break;
        }
    }
}
