<?php

namespace App\Http\Controllers;

use App\Area;
use App\Division;
use App\Office;
use App\OfficeUser;
use App\Region;
use App\Wizard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WizardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function isWizardDismissed()
    {
        $office_user = app('App\OfficeUser');
        $office_id = $office_user->whereUserId(auth()->id())->first();
        $is_dissmissed = Wizard::whereOfficeId($office_id->office_id)->first();
        $regions = Region::get(['id', 'name']);
        $areas = Area::get(['id', 'name']);
        $divisions = Division::get(['id', 'name']);
        $offices = Office::where('id', $office_id->office_id)->get(['id', 'name']);
        $is_wizard = $office_user->where('office_id', $office_id->office_id)->get()->count()??0;
        $dismiss = $is_dissmissed->dismiss??0;
        $roles = auth()->user()->roles()->get()->map(function($role){ return $role->name;})->toArray();
        $number_of_users = auth()->user()->sub_users??3;

        return response()->json([
            'dismiss' => (($is_wizard < $number_of_users) && !$dismiss && in_array('Financial advisor', $roles))?0:1,
            'sub_users' => $number_of_users,
            'default_user_count' => $is_wizard,
            'office_id' => $office_id->office_id,
            'region_drop_down' => $regions,
            'area_drop_down' => $areas,
            'divisions_drop_down' => $divisions,
            'offices_drop_down' => $offices
        ]);
    }

    public function dismissWizard(Request $request)
    {
        $wizard = new Wizard();
        $wizard->dismiss = $request->dismiss;
        $wizard->office_id = $request->office_id;
        $wizard->save();

        return response()->json(['dismiss' => $wizard->dismiss ? false : true, 'office_id' => $wizard->office_id]);
    }
}
