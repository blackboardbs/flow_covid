<?php

namespace App\Http\Controllers;

use App\Client;
use App\ClientProcess;
use App\Office;
use App\Process;
use App\ProcessGroup;
use Illuminate\Http\Request;
use App\Step;
use Illuminate\Support\Facades\Auth;

class ProcessController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function groupIndex(Request $request)
    {
        $groups = [];

        $process_type_id = $request->has('t') ? $request->input('t') : 1;

        if ($request->has('q')) {
            $process_groups = ProcessGroup::where('office_id',Auth::user()->office()->id)->where('name', 'LIKE', "%" . $request->input('q') . "%")->get();
        } else {
            $process_groups = ProcessGroup::where('office_id',Auth::user()->office()->id)->get();
        }

        foreach($process_groups as $process_group){
            $groups[][$process_group->name] = [
                'id' => $process_group->id,
                'name' => $process_group->name,
                'pcount' => Process::where('process_group_id',$process_group->id)->where('process_type_id',$process_type_id)->count(),
                'created_at' => $process_group->created_at,
                'updated_at' => $process_group->updated_at
            ];
        }

        ksort($groups);

        $groups[]['None'] = [
            'id' => '0',
            'name' => 'None',
            'pcount' => Process::where('process_group_id',0)->where('process_type_id',$process_type_id)->count(),
            'created_at' => 'N/A',
            'updated_at' => 'N/A'
        ];

        //$groups += array_splice($groups,array_search('None',array_keys($groups)),1);

        $parameters = [
            'processes' => $groups,
            'type_name' => $process_type_id == 1 ? 'Processes' : 'Related Parties Structure',
            'process_type_id' => $process_type_id
        ];

        return view('processes.groupindex')->with($parameters);
    }

    public function groupCreate(Request $request)
    {
        $process_type_id = $request->has('t') ? $request->input('t') : 1;

        $parameters = [
            'type_name' => $process_type_id == 1 ? 'Processes' : 'Related Parties Structure',
            'process_type_id' => $process_type_id
        ];

        return view('processes.groupcreate')->with($parameters);
    }

    public function groupStore(Request $request){
        $process = new ProcessGroup;
        $process->name = $request->input('name');
        $process->save();

        return redirect(route('processesgroup.index'))->with('flash_success', 'Process group created successfully.');
    }

    public function groupEdit($group_id){
        $group = ProcessGroup::where('id',$group_id)->get();

        $parameters = [
            'process_groups' => $group
        ];

        return view('processes.groupedit')->with($parameters);
    }

    public function groupUpdate(Request $request,$group_id){
        $group = ProcessGroup::find($group_id);
        $group->name = $request->input('name');
        $group->save();

        return redirect(route('processesgroup.index'))->with('flash_success', 'Process updated successfully.');
    }

    public function groupDestroy(Request $request,$group_id){

        $processes = Process::where('process_group_id',$group_id)->get();

        if($processes->count()>0){

            return redirect(route('processes.index',$group_id))->with('flash_danger', 'There are still active sub-processes assigned to this process.');
        }

        $process = ProcessGroup::find($group_id);

        $process->destroy($group_id);
        $process_type_id = $request->has('t') ? $request->input('t') : 1;

        return redirect(route('processesgroup.index', ['t' => $process_type_id]))->with(['flash_success' => 'Sub-process deleted successfully.']);
    }

    public function index(Request $request,$group_id)
    {
        $process_type_id = $request->has('t') ? $request->input('t') : 1;

        $processes = Process::with('pgroup','office.area.region.division')->where('office_id',Auth::user()->office()->id)->where('process_group_id',$group_id)->where('process_type_id', '=', $process_type_id);

        if ($request->has('q')) {
            $processes->where('name', 'LIKE', "%" . $request->input('q') . "%");
        }

        $processes = $processes->get();

        $parameters = [
            'process_group' => ($group_id == 0 ? 0 : ProcessGroup::where('id',$group_id)->first()),
            'processes' => $processes,
            'type_name' => $process_type_id == 1 ? 'Processes' : 'Related Parties Structure',
            'type_name_single' => $process_type_id == 1 ? 'Process' : 'Related Parties Structure',
            'process_type_id' => $process_type_id
        ];

        return view('processes.index')->with($parameters);
    }

    public function show($processgroup, $process, Request $request)
    {
        $process = Process::with(['steps'=> function ($q){
            $q->whereNull('deleted_at');
        }],'office.area.region.division')->where('id',$process)->first();

        $process_type_id = $request->has('t') ? $request->input('t'):1;

        return view('processes.show')->with(['processgroup'=>$processgroup,'process' => $process, 'process_type_id' => $process_type_id]);
    }

    public function create(Request $request,$group_id)
    {
        $process_type_id = $request->has('t') ? $request->input('t') : 1;

        $parameters = [
            'process_group' => $group_id,
            'process_groups' => ProcessGroup::orderBy('name')->pluck('name','id')->prepend('Please Select','0'),
            'offices' => Office::orderBy('name')->get()->pluck('name', 'id'),
            'type_name' => $process_type_id == 1 ? 'Processes' : 'Related Parties Structure',
            'process_type_id' => $process_type_id
        ];

        return view('processes.create')->with($parameters);
    }

    public function store(Request $request,$group_id)
    {
        $process = new Process;
        $process->name = $request->input('name');
        $process->office_id = $request->input('office');
        $process->process_group_id = $request->input('process');
        $process->not_started_colour = str_replace('#', '', $request->input('not_started_colour'));
        $process->started_colour = str_replace('#', '', $request->input('started_colour'));
        $process->completed_colour = str_replace('#', '', $request->input('completed_colour'));
        $process->office_id = auth()->user()->office()->id;
        $process->process_type_id = $request->has('process_type_id') ? $request->input('process_type_id') : 1;
        $process->process_group_id = $group_id;
        $process->save();

        $type_name = $request->has('process_type_id') && $request->input('process_type_id') == 2 ? 'Related Parties Structure' : 'Process';

        return redirect(route('processes.show',[$group_id,$process]))->with('flash_success', $type_name.' create successfully.');
    }

    public function edit($group_id,$process_id)
    {
        $paramaters = [
            'process' => Process::where('id',$process_id)->first(),
            'process_groups' => ProcessGroup::orderBy('name')->pluck('name','id')->prepend('Please Select','0'),
            'offices' => Office::orderBy('name')->get()->pluck('name', 'id'),
        ];

        return view('processes.edit')->with($paramaters);
    }

    public function update($process_group,$process_id, Request $request)
    {
        $process = Process::find($process_id);
        $process->name = $request->input('name');
        $process->office_id = $request->input('office');
        $process->process_group_id = $request->input('process');
        $process->not_started_colour = str_replace('#', '', $request->input('not_started_colour'));
        $process->started_colour = str_replace('#', '', $request->input('started_colour'));
        $process->completed_colour = str_replace('#', '', $request->input('completed_colour'));
        if($request->has('docfusion_process_id') && $request->input('docfusion_process_id') != ''){
            $process->docfusion_process_id = $request->input('docfusion_process_id');
        }
        if($request->has('docfusion_template_id') && $request->input('docfusion_template_id') != ''){
            $process->docfusion_template_id = $request->input('docfusion_template_id');
        }
        $process->save();

        return redirect(route('processes.show',[$process_group,$process_id]))->with('flash_success', 'Process updated successfully.');
    }

    public function destroy($group_id,Process $process, $processid, Request $request){

        if($process->clients->count()>0){

            return redirect(route('processes.show',[(isset($group_id) ? $group_id : 0),$process->id]))->with('flash_danger', 'There are still active clients assigned to that process.');
        }

        $process->destroy($processid);

        $process_type_id = $request->has('t') ? $request->input('t') : 1;
        $type_name = $process_type_id == 2 ? 'Related Parties Structure' : 'Process';

        return redirect(route('processes.index', [''=>(isset($group_id) ? $group_id : 0),'t' => $process_type_id]))->with(['flash_success' => $type_name.' deleted successfully.']);
    }

    public function processStepCount($process_id){
        $count = Step::where('process_id',$process_id)->count();

        return response()->json($count);
    }

    public function getProcessFirstStep(Request $request, $clientid,$processid){

        //$first_step = Step::where('process_id',$processid)->orderBy('order')->first();
        $first_step = ClientProcess::where('process_id',$processid)->where('client_id',$clientid)->first();

        return response()->json($first_step['step_id']);
    }

    public function getNewProcesses($clientid){

        $client = Client::find($clientid);

        return response()->json($client->startNewProcessDropdown());
    }

    public function getProcesses(){
        $process = [];
        $processes = Process::orderBy('name')->get();
        array_push($process,['id'=>'0','name'=>'Select Process']);
        foreach ($processes as $p){
            array_push($process,['id'=>$p->id,'name'=>$p->name]);
        }
        return response()->json($process);
    }

    public function getProcesses2(){


        $processes = Process::orderBy('name')->get();
        $process[0] = 'Select Process';
        foreach ($processes as $p){
            $process[$p->id] = $p->name;
        }
        return $process;
    }
}