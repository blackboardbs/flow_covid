<?php

namespace App\Http\Controllers;

use App\ActionableBooleanData;
use App\ActionableDropdownItem;
use App\ActivityRule;
use App\ClientProcess;
use App\Step;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function getRules(Request $request){

        $message = array();

        $activeprocesses = ClientProcess::select('process_id')->where('client_id',$request->input('client_id'))->get();

        $steps = Step::with('activities')->where('id',$request->input('step_id'))->get();



        foreach ($steps as $step){
            foreach ($step['activities'] as $activity){
                if(($request->has($activity->id) && $request->input($activity->id) != '') && ($request->input('old_'.$activity->id) != $request->input($activity->id))) {
                    $rules = ActivityRule::where('activity_id', $activity->id)->whereNotIn('activity_process',$activeprocesses)->get();

                    switch($activity->actionable_type){
                        case 'App\ActionableDropdown':
                            $datas = ActionableDropdownItem::whereIn('id',$request->input($activity->id))->get();

                            foreach ($datas as $data) {
                                $val = $data->name;

                                foreach ($rules as $rule) {
                                    if ($val == $rule->activity_value) {
                                        array_push($message, [$rule->id => $rule->process->name]);
                                    }
                                }
                            }
                            break;
                        case 'App\ActionableBoolean':

                                $val = $request->input($activity->id);

                                foreach ($rules as $rule) {
                                    if ($val == $rule->activity_value) {
                                        array_push($message, [$rule->id => $rule->process->name]);
                                    }
                                }
                            break;
                        default:
                            $val = $request->input($activity->id);

                            foreach ($rules as $rule) {
                                if($val == $rule->activity_value) {
                                    array_push($message, [$rule->id => $rule->process->name]);
                                }
                            }
                            break;
                    }


                }

            }
        }

        if(count($message) > 0) {
            return response()->json(['result' => 'success', 'message' => $message]);
        } else {
            return response()->json(['result' => 'error', 'message' => $message]);
        }
    }

    public function getRule(Request $request){

        $rule = ActivityRule::where('id',$request->input('id'))->first();

        return response()->json(['result'=>'success','process_id' => $rule->activity_process,'step_id' => $rule->activity_step]);
    }
}
