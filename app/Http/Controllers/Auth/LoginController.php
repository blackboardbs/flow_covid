<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\UsageLogs;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Anhskohbo\NoCaptcha;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
        /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }

    protected function validateLogin(Request $request)
    {
        $this->validate(
            $request,
            [
                'email' => 'required|string',
                'password' => 'required|string',
                //'g-recaptcha-response' => 'required|captcha'
            ]
        );
    }

    public function authenticated(Request $request, $user)
    {
        if (!$user->verified) {
            auth()->logout();
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }

        if($request->getRequestUri() === '/login') {
            $request->session()->put('platform','desktop');

            $usage = new UsageLogs();
            $usage->user_ip = $request->getClientIp();
            $usage->user_id = Auth::id();
            $usage->user_name = User::select(DB::raw("CONCAT(first_name,' ',last_name) as full_name"))->where('id',Auth::id())->first()->full_name;
            $usage->login_status = 1;
            $usage->save();

            //insert datetime and ip address into users table
            $user->update([
                'last_login_at' => Carbon::now()->toDateTimeString(),
                'last_login_ip' => $request->getClientIp()
            ]);

            return redirect()->intended($this->redirectPath());
        } else {
            $request->session()->put('platform','mobile');

            $usage = new UsageLogs();
            $usage->user_ip = $request->getClientIp();
            $usage->user_id = Auth::id();
            $usage->user_name = User::select(DB::raw("CONCAT(first_name,' ',last_name) as full_name"))->where('id',Auth::id())->first()->full_name;
            $usage->login_status = 1;
            $usage->save();

            //insert datetime and ip address into users table
            $user->update([
                'last_login_at' => Carbon::now()->toDateTimeString(),
                'last_login_ip' => $request->getClientIp()
            ]);

            return redirect(route('clients.create'));
        }
    }

public function showLoginForm(){
        return view('auth.login');
}

    protected function sendFailedLoginResponse(Request $request)
    {
        $request->session()->put('login_error', trans('auth.failed'));
        throw ValidationException::withMessages(
            [
                'error' => [trans('auth.failed')],
            ]
        );
    }
}
