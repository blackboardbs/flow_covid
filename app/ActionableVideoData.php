<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionableVideoData extends Model
{
    protected $table = 'actionable_video_data';
}
