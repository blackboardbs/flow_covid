<?php

use App\ActionableBooleanData;
use App\ActionableDateData;
use App\ActionableDocumentData;
use App\ActionableDropdownData;
use App\ActionableDropdownItem;
use App\ActionableNotificationData;
use App\ActionableTemplateEmailData;
use App\ActionableTextData;
use App\Client;
use App\ClientComment;
use App\Step;
use Illuminate\Database\Seeder;
use League\Csv\Reader;
use League\Csv\Statement;
use Carbon\Carbon;

class ClientSeeder extends Seeder
{
    //private $step;

    public function __construct() {
        /*$steps = Step::with('activities')->where('process_id',1)->get();

        foreach($steps as $key => $step){
            //Indirect modification of overloaded element of Illuminate\Database\Eloquent\Collection has no effect
            $steps[$key] = [];
            foreach($step->activities as $activity){
                $steps[$key][$activity->id] = [
                  'actionable_id' => $activity->actionable_id,
                  'actionable_type' => $activity->actionable_type
                ];
        }
        }*/
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $leads = 59;
            $prospectives = 24;
            $service_agreed = 24;
            $complete = 12;

            factory(App\Client::class, ($leads + $prospectives + $service_agreed))->create();

            $date_data = [];
            $text_data = [];
            $dropdown_data = [];
            $boolean_data = [];
            $template_email_data = [];
            for ($i = $leads + 1; $i <= $leads + $prospectives + $service_agreed; $i++) {
                foreach ([1, 2] as $item) {
                    $date_item = [
                        'actionable_date_id' => $item,
                        'data' => now(),
                        'client_id' => $i,
                        'user_id' => 1,
                        'duration' => 120,
                        'created_at' => now()
                    ];
                    array_push($date_data, $date_item);
                }

                foreach ([1, 2, 3, 4, 5] as $item) {
                    $text_item = [
                        'actionable_text_id' => $item,
                        'data' => "N/A",
                        'client_id' => $i,
                        'user_id' => 1,
                        'duration' => 120,
                        'created_at' => now()
                    ];
                    array_push($text_data, $text_item);
                }

                foreach ([1, 2, 3] as $item) {
                    $dropdown_item = [
                        'actionable_dropdown_id' => $item,
                        'actionable_dropdown_item_id' => 1,
                        'client_id' => $i,
                        'user_id' => 1,
                        'duration' => 120,
                        'created_at' => now()
                    ];
                    array_push($dropdown_data, $dropdown_item);
                }

                foreach ([1] as $item) {
                    $boolean_item = [
                        'actionable_boolean_id' => $item,
                        'data' => true,
                        'client_id' => $i,
                        'user_id' => 1,
                        'duration' => 120,
                        'created_at' => now()
                    ];
                    array_push($boolean_data, $boolean_item);
                }
            }

            for ($i = $leads + $prospectives + 1; $i <= $leads + $prospectives + $service_agreed; $i++) {
                foreach ([2, 3] as $item) {
                    $boolean_item = [
                        'actionable_boolean_id' => $item,
                        'data' => true,
                        'client_id' => $i,
                        'user_id' => 1,
                        'duration' => 120,
                        'created_at' => now()
                    ];
                    array_push($boolean_data, $boolean_item);
                }

                foreach ([3] as $item) {
                    $date_item = [
                        'actionable_date_id' => $item,
                        'data' => now(),
                        'client_id' => $i,
                        'user_id' => 1,
                        'duration' => 120,
                        'created_at' => now()
                    ];
                    array_push($date_data, $date_item);
                }

                foreach ([1] as $item) {
                    $template_email_item = [
                        'actionable_template_email_id' => $item,
                        'template_id' => 1,
                        'email' => 'test@example.com',
                        'client_id' => $i,
                        'user_id' => 1,
                        'duration' => 120,
                        'created_at' => now()
                    ];
                    array_push($template_email_data, $template_email_item);
                }
            }

            //activity type hook
            ActionableDateData::insert($date_data);
            ActionableTextData::insert($text_data);
            ActionableDropdownData::insert($dropdown_data);
            ActionableBooleanData::insert($boolean_data);
            ActionableTemplateEmailData::insert($template_email_data);


    }

    public function insertClient($record)
    {
        $step_id = 1;
        switch ($record['Current Process step']){
            case 'Leads':
                $step_id = 1;
            break;
            case 'Prospective':
                $step_id = 3;
                break;
            case 'Service Agreed':
                $step_id = 4;
            break;
            case 'Converted':
                $step_id = 100;
            break;
        }

        return Client::create([
            'first_name' => $record['Contact name'],
            'company' => $record['Client name / Company name'],
            'email' => $record['Contact Email'] ?? 'generated@example.com',
            'contact' => $record['Contact number'],
            'introducer_id' => 1,
            'office_id' => 1,
            'process_id' => 1,
            'step_id' => $step_id,
            'created_at' => ($record['Date added to lead:'] == '') ? now() : Carbon::parse($record['Date added to lead:'])
        ]);
    }

    public function insertClientHardData($client, $record)
    {
        if ($record['Client name / Company name'] != '') {
            ActionableTextData::create([
                'data' => $record['Client name / Company name'],
                'actionable_text_id' => 1,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['Lead from'] != '') {
            ActionableTextData::create([
                'data' => $record['Lead from'],
                'actionable_text_id' => 2,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['Contact name'] != '') {
            ActionableTextData::create([
                'data' => $record['Contact name'],
                'actionable_text_id' => 3,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['Contact Email'] != '') {
            ActionableTextData::create([
                'data' => $record['Contact Email'],
                'actionable_text_id' => 4,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['Contact number'] != '') {
            ActionableTextData::create([
                'data' => $record['Contact number'],
                'actionable_text_id' => 5,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['Existing client?'] == 'Y' || $record['Existing client?'] == 'N') {
            ActionableBooleanData::create([
                'data' => ($record['Existing client?']=='Y'),
                'actionable_boolean_id' => 1,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['Director'] != '') {
            ActionableDropdownData::create([
                'actionable_dropdown_id' => 3,
                'actionable_dropdown_item_id' => ActionableDropdownItem::where('actionable_dropdown_id', 3)->where('name', $record['Director'])->first()->id ?? 1,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['Fee proposal sent?'] == 'Y' || $record['Fee proposal sent?'] == 'N') {
            ActionableBooleanData::create([
                'data' => ($record['Fee proposal sent?']=='Y'),
                'actionable_boolean_id' => 3,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['Date fee proposal sent'] != '') {
            ActionableDateData::create([
                'data' => Carbon::parse($record['Date fee proposal sent']),
                'actionable_date_id' => 3,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['Meeting with BD required?'] == 'Y' || $record['Meeting with BD required?'] == 'N') {
            ActionableBooleanData::create([
                'data' => ($record['Meeting with BD required?']=='Y'),
                'actionable_boolean_id' => 5,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['LOA (Letter of Authority)'] == 'Y' || $record['LOA (Letter of Authority)'] == 'N/A') {
            ActionableTemplateEmailData::create([
                'actionable_template_email_id' => 6,
                'template_id' => 1,
                'email' => 'generated@example.com',
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['LOE (Letter of Engagement)'] == 'Y' || $record['LOE (Letter of Engagement)'] == 'N/A') {
            ActionableTemplateEmailData::create([
                'actionable_template_email_id' => 7,
                'template_id' => 1,
                'email' => 'generated@example.com',
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['LOE (Letter of Engagement)'] == 'Y') {
            ActionableTemplateEmailData::create([
                'actionable_template_email_id' => 7,
                'template_id' => 1,
                'email' => 'generated@example.com',
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['AML forms received?'] == 'Y') {
            ActionableDocumentData::create([
                'actionable_document_id' => 1,
                'document_id' => 1,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['Client Registration Form completed (CRF)'] == 'Y' || $record['Client Registration Form completed (CRF)'] == 'N/A') {
            ActionableBooleanData::create([
                'data' => ($record['Client Registration Form completed (CRF)']=='Y'),
                'actionable_boolean_id' => 6,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['Work begun / completed'] == 'Y' || $record['Work begun / completed'] == 'N/A') {
            ActionableBooleanData::create([
                'data' => ($record['Work begun / completed']=='Y'),
                'actionable_boolean_id' => 8,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['CCH code'] != '') {
            ActionableTextData::create([
                'data' => $record['CCH code'],
                'actionable_text_id' => 7,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        if ($record['Comment 1'] != '') {
            ClientComment::create([
                'client_id' => $client->id,
                'user_id' => 1,
                'comment' => $record['Comment 1']
            ]);
        }

        if ($record['Comment 2'] != '') {
            ClientComment::create([
                'client_id' => $client->id,
                'user_id' => 1,
                'comment' => $record['Comment 2']
            ]);
        }

        if ($record['Comment 3'] != '') {
            ClientComment::create([
                'client_id' => $client->id,
                'user_id' => 1,
                'comment' => $record['Comment 3']
            ]);
        }
    }

    public function progressClient($client, $record)
    {
        switch ($record['Current Process step']) {
            case 'Leads':
                break;
            case 'Prospective':
                $this->completeLead($client);
                $this->completeFeeProposal($client, $record);
                break;
            case 'Service Agreed':
                $this->completeLead($client);
                $this->completeFeeProposal($client,$record);
                $this->completeProspective($client);
                break;
            case 'Converted':
                $client->completed_at = now();
                $client->save();
            default:
                break;
        }
    }

    public function completeLead($client)
    {
        foreach ([1] as $item) {
            ActionableBooleanData::create([
                'actionable_boolean_id' => $item,
                'data' => true,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        foreach ([1, 2] as $item) {
            ActionableDateData::insert([
                'actionable_date_id' => $item,
                'data' => now(),
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        foreach ([1,2,3] as $item) {
            ActionableDropdownData::create([
                'actionable_dropdown_id' => $item,
                'actionable_dropdown_item_id' => 1,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        foreach ([1, 2, 3, 4, 5] as $item) {
            ActionableTextData::create([
                'actionable_text_id' => $item,
                'data' => 'N/A',
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }
    }

    public function completeFeeProposal($client,$record)
    {
        foreach ([2, 3] as $item) {
            ActionableBooleanData::create([
                'actionable_boolean_id' => $item,
                'data' => true,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120,
                'created_at' => ($record['Moved to prospective']!='') ? Carbon::parse($record['Moved to prospective']) : now()

            ]);
        }

        foreach ([3] as $item) {
            ActionableDateData::insert([
                'actionable_date_id' => $item,
                'data' => now(),
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        foreach ([1] as $item) {
            ActionableTemplateEmailData::create([
                'actionable_template_email_id' => $item,
                'template_id' => 1,
                'email' => 'test@example.com',
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }
    }

    public function completeProspective($client)
    {
        foreach ([4] as $item) {
            ActionableBooleanData::create([
                'actionable_boolean_id' => $item,
                'data' => true,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        foreach ([4] as $item) {
            ActionableDateData::insert([
                'actionable_date_id' => $item,
                'data' => now(),
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        foreach ([2, 3, 4] as $item) {
            ActionableTemplateEmailData::create([
                'actionable_template_email_id' => $item,
                'template_id' => 1,
                'email' => 'test@example.com',
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }
    }

    public function completeServiceAgreed($client)
    {
        foreach ([5,6,7,8] as $item) {
            ActionableBooleanData::create([
                'actionable_boolean_id' => $item,
                'data' => true,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        foreach ([1] as $item) {
            ActionableDocumentData::create([
                'actionable_document_id' => $item,
                'document_id' => 1,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        foreach ([4,5,6,7] as $item) {
            ActionableDropdownData::create([
                'actionable_dropdown_id' => $item,
                'actionable_dropdown_item_id' => 1,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        foreach ([1] as $item) {
            ActionableNotificationData::create([
                'actionable_notification_id' => $item,
                'notification_id' => 1,
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        foreach ([5,6,7] as $item) {
            ActionableTemplateEmailData::create([
                'actionable_template_email_id' => $item,
                'template_id' => 1,
                'email' => 'test@example.com',
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }

        foreach ([6] as $item) {
            ActionableTextData::create([
                'actionable_text_id' => $item,
                'data' => 'N/A',
                'client_id' => $client->id,
                'user_id' => 1,
                'duration' => 120
            ]);
        }
    }
}
